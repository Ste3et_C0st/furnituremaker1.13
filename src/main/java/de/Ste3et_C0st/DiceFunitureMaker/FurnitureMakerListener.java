package de.Ste3et_C0st.DiceFunitureMaker;

import java.util.Objects;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;

public class FurnitureMakerListener implements Listener{
	
	@EventHandler
	public void onScorll(PlayerItemHeldEvent e){
		FurnitureMaker maker = FurnitureMakerPlugin.getInstance().getManager().getMaker(e.getPlayer());
		if(Objects.isNull(maker)) return;
		if(!e.getPlayer().equals(maker.getPlayer())) return;
		ItemStack itemStack = e.getPlayer().getInventory().getItem(e.getPreviousSlot());
		if(itemStack==null) return;
		if(!maker.isItem(itemStack)) return;
		if(!e.isCancelled()) {
			if(e.getPlayer().isSneaking()) {
				int newSlot = e.getNewSlot();
				int oldSlot = e.getPreviousSlot();
				if(newSlot != oldSlot) {
					boolean forward = newSlot > oldSlot;
					boolean cancle = false;
					if(forward) {
						cancle = maker.onItemSlotForward(itemStack);
					}else {
						cancle = maker.onItemSlotBackward(itemStack);
					}
					if(cancle) e.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		FurnitureMaker maker = FurnitureMakerPlugin.getInstance().getManager().getMaker(e.getPlayer());
		if(Objects.isNull(maker)) {return;}
		if(Objects.isNull(e.getItem())) {return;}
		if(!e.getHand().equals(EquipmentSlot.HAND)) return;
		if(maker.isItem(e.getItem())) {
			if (e.getHand() == EquipmentSlot.HAND) {
				e.setCancelled(true);
				if(e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
					maker.onItemStackLeftClick(e.getItem());
				}else if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
					maker.onItemStackRightClick(e.getItem());
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerDropItemEvent e){
		FurnitureMaker maker = FurnitureMakerPlugin.getInstance().getManager().getMaker(e.getPlayer());
		if(Objects.isNull(maker)) {return;}
		if(Objects.isNull(e.getItemDrop())) {return;}
		if(maker.isItem(e.getItemDrop().getItemStack())) {
			e.setCancelled(true);
			maker.onItemDrop(e.getItemDrop().getItemStack());
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		FurnitureMaker maker = FurnitureMakerPlugin.getInstance().getManager().getMaker(e.getPlayer());
		if(Objects.isNull(maker)) {return;}
		Block b = e.getBlock();
		if(maker.getArea().isInside(b.getLocation())) {
			maker.getBlockList().add(b);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		FurnitureMaker maker = FurnitureMakerPlugin.getInstance().getManager().getMaker(e.getPlayer());
		if(Objects.isNull(maker)) {return;}
		Block b = e.getBlock();
		if(maker.getArea().isInside(b.getLocation())) {
			maker.getBlockList().remove(b);
		}
	}
	
}
