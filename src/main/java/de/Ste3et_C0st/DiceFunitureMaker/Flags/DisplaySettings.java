package de.Ste3et_C0st.DiceFunitureMaker.Flags;

import java.awt.Color;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.bukkit.entity.Player;
import org.bukkit.entity.TextDisplay.TextAlignment;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Display.Billboard;
import org.bukkit.entity.Display.Brightness;
import org.bukkit.entity.ItemDisplay.ItemDisplayTransform;
import org.bukkit.event.inventory.InventoryType;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ChatInputHandler;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ChatInputHandler.ReturnState;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.ClickGui;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.GuiButton;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.entity.fDisplay;
import de.Ste3et_C0st.FurnitureLib.main.entity.fItem_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fText_display;

public class DisplaySettings extends ClickGui{

	private int glow_override = -1, lineWidth = 200, skyLight = -1, blockLight = -1;
	private float shadowStrenght = 0f, shadowRadius = 1f;
	private Billboard board = Billboard.FIXED;
	private AtomicInteger slotncounter = new AtomicInteger();
	//org.bukkit.entity.TextDisplay.TextAligment
	private TextAlignment aligment = TextAlignment.CENTER;
	private boolean seeThrough = false, shadowed = false, defaultBackground = false;
	private String name = "";
	private ItemDisplayTransform itemDisplayTransform = ItemDisplayTransform.FIXED;
	
	private int color = -1;
	
	public DisplaySettings(Player player) {
		super(54, "Change Display Settings", InventoryType.CHEST, player, FurnitureLib.getInstance());
		this.fillComplete();
		this.initSettings();
		this.setContent();
		this.open(player);
	}
	
	private void initSettings() {
		Optional<fDisplay> optDisplay = getMaker().getEntityList().stream().filter(Objects::nonNull).filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fDisplay.class::cast).findFirst();
		if(optDisplay.isPresent()) {
			fDisplay display = optDisplay.get();
			this.glow_override = display.getGlowOverride();
			this.shadowRadius = display.getShadowRadius();
			this.shadowStrenght = display.getShadowStrength();
			this.board = display.getBillboard();
			
			final Brightness brightness = display.getBrightnessObject();
			this.skyLight = brightness.getSkyLight();
			this.blockLight = brightness.getBlockLight();
			this.name = display.getCustomName();
			
			if(display instanceof fText_display) {
				fText_display text = fText_display.class.cast(display);
				this.lineWidth = text.getLineWidth();
				this.aligment = text.getTextAligment();
				this.seeThrough = text.isSeeThrough();
				this.shadowed = text.isShadowed();
				this.defaultBackground = text.isDefaultBackground();
				this.color = text.getBackgroundColorInt();
			}
			
			if(display instanceof fItem_display itemDisplay) {
				this.itemDisplayTransform = itemDisplay.getItemDisplay();
			}
		}
	}
	
	private void setContent() {
		this.setBillboardButton();
		this.setShadowStrenghtButton();
		this.setShadowRadiusButton();
		
		this.setSkyLight();
		this.setBlockLight();
		
		if(getMaker().getSelectedEntityType().equals(fText_display.class)) {
			this.setLineWidth();
			this.setTextDisplayButton();
			this.setShadowed();
			this.setSeeThrough();
			this.setDefaultBackground();
			this.setBackgroundColor();
		}
		
		if(getMaker().getSelectedEntityType().equals(fItem_display.class)) {
			this.setItemDisplay();
			this.setFunctionButton();
		}
	}
	
	private void setBackgroundColor() {
		net.md_5.bungee.api.ChatColor chColor = net.md_5.bungee.api.ChatColor.of(new Color(color, true));
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.DIAMOND).setName("§7BackgroundColor: " + chColor + "■").setLore("§7ColorValue: " + chColor + color), event -> {
		    final String hex_regex = "^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6})$";
			new ChatInputHandler(getPlayer(), string -> {
				return string.matches(hex_regex);
			}, 
			onFinish -> {
				if(onFinish.getState() == ReturnState.SUCCESS) {
					
					final String colorStr = onFinish.getInput();
					
					final int r = Integer.parseInt(colorStr.substring(1, 3), 16);
					final int g = Integer.parseInt(colorStr.substring(3, 5), 16);
					final int b = Integer.parseInt(colorStr.substring(5, 7), 16);
					
					int alpha = 255;
					
					if (colorStr.length() > 7) alpha = Integer.parseInt(colorStr.substring(7, 9), 16);
		            
					final Color color = new Color(r, g, b, alpha);
					
					this.color = color.getRGB();
					
					getPlayer().sendMessage("§fYou Changed the Background color to: " + net.md_5.bungee.api.ChatColor.of(color) + "■");
					this.apply();
				}else if(onFinish.getState() == ReturnState.WRONG) {
					getPlayer().sendMessage("§cYour Color code is wrong.");
				}
			},
			onOpen -> {
				getPlayer().sendMessage("§fPlease insert §aHex ColorCode §for §aRGBA Hex");
				getPlayer().sendMessage("§f#§aff§dff§eff");
				getPlayer().sendMessage("§f#§aff§dff§eff§cff");
				getPlayer().sendMessage("§fYou have 20 Secounds");
				getPlayer().closeInventory();
			}, 
			
		    Duration.ofSeconds(20));
		}));
	}
	
	private int processIntClick(int value, int max, int min, boolean add, boolean fast) {
		final int newValue = add ? value + (fast ? 10 : 1) : value - (fast ? 10 : 1);
		return newValue < min ? min : newValue > max ? max : newValue;
	}
	
	private float processFloatClick(float value, float max, float min, boolean add, boolean fast) {
		final float newValue = add ? value + (fast ? 1f : .5f) : value - (fast ? 1f : .5f);
		return newValue < min ? min : newValue > max ? max : newValue;
	}
	
	private void setLineWidth() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.KNOWLEDGE_BOOK).setName("§7LineWidth: §a" + this.lineWidth), event -> {
			final int value = this.processIntClick(this.lineWidth, 1000, 1, event.isLeftClick(), event.isShiftClick());
			if(value != this.lineWidth) {
				this.lineWidth = value;
				this.apply();
			}
		}));
	}
	
	private void setBlockLight() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.GLOWSTONE).setName("§7BlockLight: §a" + blockLight), event -> {
			final int value = this.processIntClick(this.blockLight, 15, 0, event.isLeftClick(), event.isShiftClick());
			if(value != this.blockLight) {
				this.blockLight = value;
				this.apply();
			}
		}));
	}
	
	private void setSkyLight() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.LIGHT).setName("§7SkyLight: §a" + skyLight), event -> {
			final int value = this.processIntClick(this.skyLight, 15, 0, event.isLeftClick(), event.isShiftClick());
			if(value != this.skyLight) {
				this.skyLight = value;
				this.apply();
			}
		}));
	}
	
	private void setFunctionButton() {
		
		this.addButton(this.getLastSlot(), new GuiButton(ItemStackBuilder.of(Material.ACACIA_BOAT).setName("§7Mount " + (this.name.startsWith("#SITZ") ? "§atrue" : "§cfalse")).setLore(this.name), event -> {
			this.name = this.name.equalsIgnoreCase("#SITZ") ? "" : "#SITZ#";
			this.apply();
		}));
		
		this.addButton(this.getLastSlot() - 1, new GuiButton(ItemStackBuilder.of(Material.ITEM_FRAME).setName("§7ItemHolder " + (this.name.startsWith("#ITEM") ? "§atrue" : "§cfalse")).setLore(this.name), event -> {
			this.name = this.name.equalsIgnoreCase("#ITEM#") ? "" : "#ITEM#";
			this.apply();
		}));
	}
	
	private void setItemDisplay() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.DIAMOND).setName("§7ItemDisplay").setLore(getEnumLore(ItemDisplayTransform.values(), this.itemDisplayTransform)), event -> {
			if(event.isLeftClick()) {
				itemDisplayTransform = itemDisplayTransform.ordinal() + 1 < ItemDisplayTransform.values().length ? ItemDisplayTransform.values()[itemDisplayTransform.ordinal() + 1] : ItemDisplayTransform.NONE;
			}else {
				itemDisplayTransform = itemDisplayTransform.ordinal() > 0 ? ItemDisplayTransform.values()[itemDisplayTransform.ordinal() - 1] : ItemDisplayTransform.FIXED;
			}
			this.apply();
		}));
	}
	
	private void setDefaultBackground() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.GLOW_ITEM_FRAME).setName("§7DefaultBackground: §a" + this.defaultBackground), event -> {
			this.defaultBackground = !this.defaultBackground;
			this.apply();
		}));
	}
	
	private void setShadowed() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.INK_SAC).setName("§7Shadowed: §a" + this.shadowed), event -> {
			this.shadowed = !this.shadowed;
			this.apply();
		}));
	}
	
	private void setSeeThrough() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.GLASS_BOTTLE).setName("§7See Through: §a" + this.seeThrough), event -> {
			this.seeThrough = !this.seeThrough;
			this.apply();
		}));
	}
	
	private void setTextDisplayButton() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.ENCHANTED_BOOK).setName("§7Text Aligment").setLore(getEnumLore(TextAlignment.values(), this.aligment)), event -> {
			if(event.isLeftClick()) {
				aligment = aligment.ordinal() + 1 < TextAlignment.values().length ? TextAlignment.values()[aligment.ordinal() + 1] : TextAlignment.CENTER;
			}else {
				aligment = aligment.ordinal() > 0 ? TextAlignment.values()[aligment.ordinal() - 1] : TextAlignment.RIGHT;
			}
			this.apply();
		}));
	}
	
	private void setBillboardButton() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.PAINTING).setName("§7Billboard").setLore(this.getBillboardLore()), event -> {
			if(event.isLeftClick()) {
				board = board.ordinal() + 1 < Billboard.values().length ? Billboard.values()[board.ordinal() + 1] : Billboard.FIXED;
			}else {
				board = board.ordinal() > 0 ? Billboard.values()[board.ordinal() - 1] : Billboard.CENTER;
			}
			this.apply();
		}));
	}
	
	private List<String> getEnumLore(Enum[] enumArray, Enum selected){
		List<String> loreList = new ArrayList<String>();
		
		Stream.of(enumArray).forEach(entry -> {
			loreList.add((entry == selected ? ChatColor.GREEN : ChatColor.GRAY) + "- " + entry.name());
		});
		
		return loreList;
	}
	
	private List<String> getBillboardLore(){
		return getEnumLore(Billboard.values(), this.board);
	}
	
	private void setShadowStrenghtButton() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.COAL).setName("§7Shadow Strength: §a" + this.shadowStrenght).setLore(""), event -> {
			final float value = this.processFloatClick(this.shadowStrenght, 64, 0, event.isLeftClick(), event.isShiftClick());
			if(value != this.shadowStrenght) {
				this.shadowStrenght = value;
				this.apply();
			}
		}));
	}
	
	private void setShadowRadiusButton() {
		this.addButton(slotncounter.getAndIncrement(), new GuiButton(ItemStackBuilder.of(Material.BLACK_WOOL).setName("§7Shadow Radius: §e" + this.shadowRadius).setLore(""), event -> {
			final float value = this.processFloatClick(this.shadowRadius, 64, 0, event.isLeftClick(), event.isShiftClick());
			if(value != this.shadowRadius) {
				this.shadowRadius = value;
				this.apply();
			}
		}));
	}
	
	public void apply() {
		getMaker().getEntityList().stream().filter(Objects::nonNull).filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fDisplay.class::cast).forEach(entry -> {
			entry.setBillboard(board);
			entry.setShadowRadius(fixFloat(shadowRadius));
			entry.setShadowStrength(fixFloat(shadowStrenght));
			entry.setBrightness(new Brightness(blockLight, skyLight));
			if(entry instanceof fText_display) {
				fText_display text_display = fText_display.class.cast(entry);
				text_display.setLineWidth(lineWidth);
				text_display.setAlignment(aligment);
				text_display.setSeeThrough(seeThrough);
				text_display.setShadowed(shadowed);
				text_display.setDefaultBackground(defaultBackground);
				text_display.setBackgroundColor(this.color);
			}
			
			if(entry instanceof fItem_display itemDisplay) {
				itemDisplay.setItemDisplay(itemDisplayTransform);
				entry.setCustomName(this.name);
			}
			
			entry.update(getPlayer());
		});
		new DisplaySettings(getPlayer());
	}
	
	public float fixFloat(float f) {
		return (float) (Math.round(f * 100.0) / 100.0);
	}
	
	private FurnitureMaker getMaker() {
		return FurnitureMakerPlugin.getInstance().getManager().getMaker(getPlayer());
	}

}
