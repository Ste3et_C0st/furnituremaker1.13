package de.Ste3et_C0st.DiceFunitureMaker.Flags;

import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.CallbackGUI;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.ClickGui;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.GuiButton;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.entity.fItem_display;

public class ItemDisplayInventory extends ClickGui{

	public ItemDisplayInventory(Player p) {
		super(27, "Set ItemDisplay ItemStack", InventoryType.CHEST, p, FurnitureLib.getInstance());
		this.setContent();
		
		this.onClick(new CallbackGUI() {
			@Override
			public void onResult(ItemStack stack, Integer slot) {
				if(this.getInventoryPos() == ClickedInventory.BOTTOM) {
					if(stack.getType().isAir() == false) {
						getMaker().getEntityList().stream().filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fItem_display.class::cast).forEach(entry -> {
							entry.setItemStack(stack.clone());
							entry.update(getPlayer());
						});
						new ItemDisplayInventory(getPlayer());
					}
				}
			}
		});
		
		this.open(getPlayer());
	}

	private void setContent() {
		this.fillComplete();
		this.addButton(13, new GuiButton(getStack(), event -> {
			getMaker().getEntityList().stream().filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fItem_display.class::cast).forEach(entry -> {
				entry.setItemStack(new ItemStack(Material.AIR));
				entry.update(getPlayer());
			});
			new ItemDisplayInventory(getPlayer());
		}));
	}
	
	private ItemStack getStack() {
		Optional<fItem_display> opt = getMaker().getEntityList().stream().filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fItem_display.class::cast)
				.filter(entry -> entry.getStack().getType().isAir() == false)
				.findFirst();

		if(opt.isPresent()) {
			return opt.get().getStack().clone();
		}
		
		return ItemStackBuilder.of(Material.BARRIER).setName("§cMaterial is missing!").setLore("§7Pickup an item from BottomInventory").build();
		
	}
	
	private FurnitureMaker getMaker() {
		return FurnitureMakerPlugin.getInstance().getManager().getMaker(getPlayer());
	}
	
}
