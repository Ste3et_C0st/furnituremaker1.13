package de.Ste3et_C0st.DiceFunitureMaker.Flags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMakerInventory;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.entity.fArmorStand;
import de.Ste3et_C0st.FurnitureLib.main.entity.fBlock_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fContainerEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fInteraction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fItem_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fText_display;

public class ArmorStandSelector implements Listener{

	private Player p;
	private ObjectID id;
	private boolean enable = true, multiSelect = false;
	private Inventory inv = null;
	private int perPage = 45, side = 1, maxSide = 0;
	private ItemStack stack1,stack2,stack3,stack4,stack5,stack6,stack7;
	private FurnitureMakerInventory model;
	private final Class<? extends fEntity> entityClasss;
	
	public ArmorStandSelector(FurnitureMakerInventory model, Class<? extends fEntity> entityClasss){
		this.id = model.getObjectID();
		this.p = model.getPlayer();
		this.entityClasss = entityClasss;
		this.model = model;
		this.inv = Bukkit.createInventory(null, perPage+9, "Entity Selector");
		this.setButtons();
		this.setContent();
		this.p.openInventory(inv);
		Bukkit.getPluginManager().registerEvents(this, FurnitureMakerPlugin.getInstance());
	}
	
	public void setButtons() {
		stack1=new ItemStack(Material.TIPPED_ARROW, 1);
		stack3=new ItemStack(Material.TIPPED_ARROW, 1);
		stack2=new ItemStack(Material.PAPER);
		stack4 = new ItemStackBuilder(Material.BARRIER).setName("§cRemove Selected Entities").build();
		stack7 = new ItemStackBuilder(Material.END_CRYSTAL).setName("Clone Selected Entities").build();
		stack5 = new ItemStackBuilder(Material.valueOf(FurnitureLib.isNewVersion() ? "STONE_SLAB" : "STEP")).setName("§7Select Mode: §2Single Select").build();
		stack6=  new ItemStackBuilder(Material.EMERALD).setName("§6Multiselect Tool").setLore(Arrays.asList("§eLeftclick: §6Select All Entities", "§eRightclick: §cDeselect All Entities")).build();
		
		PotionMeta meta = (PotionMeta) stack1.getItemMeta();
		meta.setBasePotionData(new PotionData(PotionType.REGEN));
		meta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 1, 1), true);
		meta.setDisplayName("§cPrev Page");
		stack1.setItemMeta(meta);
		
		meta = (PotionMeta) stack3.getItemMeta();
		meta.setBasePotionData(new PotionData(PotionType.LUCK));
		meta.addCustomEffect(new PotionEffect(PotionEffectType.LUCK, 1, 1), true);
		meta.setDisplayName("§2Next Page");
		stack3.setItemMeta(meta);
		
		double d =(double) this.id.getPacketList().size();
		double b =(double) perPage;
		double l = Math.ceil(d/b);
		int u = (int) l;
		this.maxSide = u;
		ItemMeta m = stack2.getItemMeta();
		m.setDisplayName("§6Page[§3" + this.side + "§6/§3" + u + "§6]");
		stack2.setItemMeta(m);
		
		inv.setItem(inv.getSize()-6, stack1);
		inv.setItem(inv.getSize()-5, stack2);
		inv.setItem(inv.getSize()-4, stack3);
		inv.setItem(inv.getSize()-9, stack4);
		inv.setItem(inv.getSize()-1, stack5);
		
		inv.setItem(inv.getSize()-7, stack7);
	}
	
	public void setContent() {
		AtomicInteger slot = new AtomicInteger(0);
		this.id.getPacketList().stream().filter(entry -> entry.getClass().equals(this.entityClasss)).skip((side - 1) * perPage).limit(perPage).forEach(entiyt -> {
			
			ItemStack stack = new ItemStack(Material.ARMOR_STAND);
			
			if(entiyt instanceof fContainerEntity) {
				fContainerEntity containerEntity = fContainerEntity.class.cast(entiyt);
				final Optional<ItemStack> optStack = Arrays.asList(containerEntity.getInventory().getIS()).stream()
						.filter(Objects::nonNull).filter(s -> !s.getType().equals(Material.AIR)).findFirst();
				
				if(optStack.isPresent()) {
					stack = optStack.get().clone();
				}
			}
			
			if(entiyt instanceof fBlock_display) {
				fBlock_display display = fBlock_display.class.cast(entiyt);
				final Material material = display.getBlockData().getMaterial().isAir() ? Material.STONE : display.getBlockData().getMaterial();
				stack.setType(material);
			}
			
			if(entiyt instanceof fItem_display) {
				final ItemStack itemStack = fItem_display.class.cast(entiyt).getStack().clone();
				stack = Objects.isNull(itemStack) || itemStack.getType().isAir() ? new ItemStack(Material.DIAMOND_SWORD) : itemStack.clone();
			} 
			
			if(entiyt instanceof fText_display) {
				stack.setType(Material.NAME_TAG);
			}
			
			if(entiyt instanceof fInteraction) {
				stack.setType(Material.TARGET);
			}
			
			ItemMeta smeta = stack.getItemMeta();
			smeta.setDisplayName("§1Entitie: [§4" + entiyt.getEntityID() + "§1]");
			if(model.getEntityList().contains(entiyt)){smeta.addEnchant(Enchantment.KNOCKBACK, 1, false);}
			
			List<String> lore = new ArrayList<String>();
			lore.add("§8- " + entiyt.getEntityType().name());
			lore.add("§6Metadata:");
			lore.add(getInfo("§7- Invisible",entiyt.isInvisible()));
			lore.add(getInfo("§7- Fire",entiyt.isFire()));
			
			if(entiyt instanceof fArmorStand){
				fArmorStand as = (fArmorStand) entiyt;
				lore.add(getInfo("§7- Small",as.isSmall()));
				lore.add(getInfo("§7- Arms",as.hasArms()));
				lore.add(getInfo("§7- BasePlate",as.hasBasePlate()));
				lore.add(getInfo("§7- Marker",as.isMarker()));
			}	
		
			lore.add(getInfo("§7- Name", entiyt.getName()));
			lore.add("§6Inventory:");
			if(entiyt instanceof fContainerEntity) {
				fContainerEntity containerEntity = fContainerEntity.class.cast(entiyt);
				lore.add(getInfo("§7- Main Hand", containerEntity.getItemInMainHand()));
				lore.add(getInfo("§7- Off Hand", containerEntity.getItemInOffHand()));
				lore.add(getInfo("§7- Helmet", containerEntity.getHelmet()));
			}
			
			smeta.setLore(lore);
			stack.setItemMeta(smeta);
			inv.setItem(slot.getAndIncrement(), stack);
		});
	}
	
	public String getInfo(String s, ItemStack stack){
		if(stack==null||stack.getType()==null||stack.getType().equals(Material.AIR)) return s + ":§c " + Material.AIR.name();
		return s + ":§a " + stack.getType().name();
	}
	
	public String getInfo(String s, boolean b){
		if(b) return s + ":§a true";
		return s + ":§c false";
	}
	public String getInfo(String s, String l){
		try{
			if( l == null || l.isEmpty() || l.equalsIgnoreCase("") || l.startsWith("#Mount:") || l.startsWith("#Light:") || l.startsWith("#Inventory:")) return s + ":§c NA";
			
			return s + ": " + ChatColor.translateAlternateColorCodes('&', l);
		}catch(Exception e){
			 return s + ":§c NA";
		}
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e){
		if(e.getClickedInventory()==null)return;
		if(setEnable(false)) return;
		if(!e.getClickedInventory().equals(inv)) return;
		e.setCancelled(true);
		if(model.getEntityList().isEmpty()) return;
		if(e.getCurrentItem()==null) return;
		if(e.getCurrentItem().getType()==null) return;
		
		if(e.getSlot() >= 45) {
			if(e.getCurrentItem().getType().equals(Material.TIPPED_ARROW)) {
				if(e.getSlot()==inv.getSize()-6){
					if(side==1) return;
					this.side = this.side - 1;
					this.inv.clear();
					this.setButtons();
					this.setContent();
					p.updateInventory();
				}else if(e.getSlot()==inv.getSize()-4){
					if(side+1>maxSide) return;
					this.side = this.side + 1;
					this.inv.clear();
					this.setButtons();
					this.setContent();
					p.updateInventory();
				}
			}else if(e.getCurrentItem().getType().equals(Material.BARRIER)) {
				model.getEntityList().stream().filter(Objects::nonNull).forEach(model::removeEntity);
				model.getEntityList().clear();
				p.closeInventory();
				this.setEnable(false);
				this.model.selectFirstClass(this.model.getSelectedEntityType());
			}else if(e.getCurrentItem().getType().equals(Material.EMERALD)) {
				if(multiSelect){
					if(e.getClick().equals(ClickType.RIGHT)){
						this.model.selectFirstClass(this.model.getSelectedEntityType());
						this.inv.clear();
						this.setButtons();
						this.setContent();
						if(!multiSelect){p.closeInventory();}else{p.updateInventory();}
					}else{
						this.model.selectAllByClass(this.model.getSelectedEntityType());
						this.inv.clear();
						this.setButtons();
						this.setContent();
					}
				}
			}else if(e.getCurrentItem().getType().equals(Material.BRICK)) {
				if(multiSelect){
					ItemStack stack = new ItemStack(Material.valueOf(FurnitureLib.isNewVersion() ? "STONE_SLAB" : "STEP"));
					ItemMeta im = stack.getItemMeta();
					im.setDisplayName("§7Select Mode: §2Single Select");
					stack.setItemMeta(im);
					inv.setItem(inv.getSize()-1, stack);
					inv.setItem(inv.getSize()-2, new ItemStack(Material.AIR));
					p.updateInventory();
					multiSelect=false;
				}
			}else if(e.getCurrentItem().getType().name().equalsIgnoreCase(FurnitureLib.isNewVersion() ? "STONE_SLAB" : "STEP")) {
				if(!multiSelect){
					ItemStack stack = new ItemStack(Material.BRICK);
					ItemMeta im = stack.getItemMeta();
					im.setDisplayName("§7Select Mode: §6Multi Select");
					stack.setItemMeta(im);
					inv.setItem(inv.getSize()-1, stack);
					inv.setItem(inv.getSize()-2, stack6);
					p.updateInventory();
					multiSelect=true;
				}
			}else if(e.getCurrentItem().getType().equals(Material.END_CRYSTAL)) {
				HashSet<fEntity> cloneList = new HashSet<fEntity>();
				model.getEntityList().forEach(entity -> {
					if(Objects.nonNull(entity)) {
						fEntity clone = entity.clone();
						clone.setLocation(entity.getLocation());
						cloneList.add(clone);
					}
				});
				model.getObjectID().addEntities(cloneList);
				model.select(cloneList);
				new ArmorStandSelector(model, this.entityClasss);
			}
		} else {
			int i = check(e.getCurrentItem());
			int j = e.getRawSlot();
			ItemStack iS = e.getCurrentItem();
			ItemMeta meta = iS.getItemMeta();
			if(meta.hasEnchants()){
				if(model.getEntityList().size() > 1) {
					fEntity selected = getSelected(i);
					if(Objects.nonNull(selected)) {
						if(model.getEntityList().contains(selected)) {
							model.deSelect(selected);
							for(Enchantment enchant : meta.getEnchants().keySet()){meta.removeEnchant(enchant);}
							iS.setItemMeta(meta);
							inv.setItem(j, iS);
							p.updateInventory();
						}
					}
				}
			}else{
				fEntity fEntity = getEntity(i);
				if(Objects.nonNull(fEntity)) {
					if(!multiSelect){
						p.closeInventory();
						model.select(fEntity);
					}else{
						model.selectAdd(fEntity);
						meta.addEnchant(Enchantment.KNOCKBACK, 1, false);
						iS.setItemMeta(meta);
						inv.setItem(j, iS);
					}
				}
			}
		}
	}
	
	private fEntity getSelected(int i) {
		return model.getEntityList().stream().filter(entity -> entity.getEntityID() == i).findFirst().orElse(null);
	}
	
	private fEntity getEntity(int i) {
		return model.getObjectID().getPacketList().stream().filter(entity -> entity.getEntityID() == i).findFirst().orElse(null);
	}
	
	public int check(ItemStack stack){
		int i = 0;
		if(!stack.hasItemMeta()) return i;
		if(!stack.getItemMeta().hasDisplayName()) return i;
		String s = stack.getItemMeta().getDisplayName();
		s = ChatColor.stripColor(s);
		s = s.replace("Entitie: [", "");
		s = s.replace("]", "");
		i = Integer.parseInt(s);
		return i;
	}
	
	public void getStand(int i){
		this.p.closeInventory();
		setEnable(false);
		fEntity stand = null;
		for(fEntity s : this.model.getObjectID().getPacketList()){
			if(s!=null){
				if(s.getEntityID()==i){
					stand = s;
					break;
				}
			}
		}
		this.model.highLightEntities(stand);
	}

	public boolean isEnable() {
		return enable;
	}

	public boolean setEnable(boolean enable) {
		this.enable = enable;
		return enable;
	}
}
