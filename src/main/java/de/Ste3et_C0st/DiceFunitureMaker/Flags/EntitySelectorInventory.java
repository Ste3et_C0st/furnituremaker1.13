package de.Ste3et_C0st.DiceFunitureMaker.Flags;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.ClickGui;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.GuiButton;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.entity.fArmorStand;
import de.Ste3et_C0st.FurnitureLib.main.entity.fBlock_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fInteraction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fItem_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fText_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.monster.fMagmaCube;

public class EntitySelectorInventory extends ClickGui{

	public EntitySelectorInventory(Player player) {
		super(36, "Pick an EntityType to edit", InventoryType.CHEST, player, FurnitureLib.getInstance());
		this.setContent();
		this.open(player);
	}
	
	private void setContent() {
		this.fillComplete();
		final FurnitureMaker maker = this.getMaker();
		final AtomicInteger counter = new AtomicInteger(10);
		
		this.addStack(fArmorStand.class, counter, maker.getItem(1, 0), 1);
		this.addStack(fBlock_display.class, counter, maker.getItem(3, 0), 3);
		this.addStack(fText_display.class, counter, maker.getItem(4, 0), 4);
		this.addStack(fItem_display.class, counter, maker.getItem(5, 0), 5);
		this.addStack(fInteraction.class, counter, maker.getItem(6, 0), 6);
		
		counter.set(19);
		this.addStack(fMagmaCube.class, counter, maker.getItem(7, 0), 7);
	}
	
	private void addStack(Class<? extends fEntity> entity, AtomicInteger slot, Optional<ItemStack> spawnStack, int side) {
		if(spawnStack.isPresent()) {
			final ItemStack stack = spawnStack.get().clone();
			final String name = stack.getItemMeta().getDisplayName().replaceFirst("Spawn ", "");
			
			ItemStackBuilder builder = ItemStackBuilder.of(stack).setLore(
					"§7Entitys: " + getMaker().getEntitySize(entity),
					"§9Click to edit " + name + " Entities"
			);
			
			builder.setName(name);
			
			this.addButton(slot.incrementAndGet(), new GuiButton(builder, event -> {
				this.close();
				this.getMaker().giveSide(side, entity);
			}));
		}
	}
	
	private FurnitureMaker getMaker() {
		return FurnitureMakerPlugin.getInstance().getManager().getMaker(getPlayer());
	}
}
