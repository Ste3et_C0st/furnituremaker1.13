package de.Ste3et_C0st.DiceFunitureMaker.Flags;

import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.CallbackGUI;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.ClickGui;
import de.Ste3et_C0st.FurnitureLib.Utilitis.inventory.manage.GuiButton;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.entity.fBlock_display;

public class BlockDisplayInventory extends ClickGui{

	public BlockDisplayInventory(Player p) {
		super(27, "Set Display BlockData", InventoryType.CHEST, p, FurnitureLib.getInstance());
		this.setContent();
		
		this.onClick(new CallbackGUI() {
			@Override
			public void onResult(ItemStack stack, Integer slot) {
				if(this.getInventoryPos() == ClickedInventory.BOTTOM) {
					if(stack.getType().isBlock()) {
						getMaker().getEntityList().stream().filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fBlock_display.class::cast).forEach(entry -> {
							entry.setBlockData(stack.getType());
							entry.update(getPlayer());
						});
						new BlockDisplayInventory(getPlayer());
					}
				}
			}
		});
		
		this.open(getPlayer());
	}

	private void setContent() {
		this.fillComplete();
		this.addButton(13, new GuiButton(getStack(), event -> {
			getMaker().getEntityList().stream().filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fBlock_display.class::cast).forEach(entry -> {
				entry.setBlockData(Material.AIR);
				entry.update(getPlayer());
			});
			new BlockDisplayInventory(getPlayer());
		}));
	}
	
	private ItemStack getStack() {
		Optional<fBlock_display> opt = getMaker().getEntityList().stream().filter(entry -> entry.getClass().equals(getMaker().getSelectedEntityType())).map(fBlock_display.class::cast)
				.filter(entry -> entry.getBlockData().getMaterial().isAir() == false)
				.filter(entry -> entry.getBlockData().getMaterial().isBlock())
				.findFirst();

		if(opt.isPresent()) {
			return new ItemStack(opt.get().getBlockData().getMaterial());
		}
		
		return ItemStackBuilder.of(Material.BARRIER).setName("§cMaterial is missing!").setLore("§7Pickup an blockitem from BottomInventory").build();
	}
	
	private FurnitureMaker getMaker() {
		return FurnitureMakerPlugin.getInstance().getManager().getMaker(getPlayer());
	}
	
}
