package de.Ste3et_C0st.DiceFunitureMaker;

import java.util.HashSet;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;

public class FurnitureMakerManager {

	public HashSet<FurnitureMaker> furnitureMakerSet = new HashSet<FurnitureMaker>();
	
	public FurnitureMakerManager() {
		Bukkit.getPluginManager().registerEvents(new FurnitureMakerListener(), FurnitureMakerPlugin.getInstance());
	}
	
	public FurnitureMaker getMaker(Player player) {
		return furnitureMakerSet.stream().filter(maker -> Objects.nonNull( maker.getPlayer()) && maker.getPlayer().equals(player)).findFirst().orElse(null);
	}
	
	public boolean isInsideMaker(Player player) {
		return Objects.nonNull(this.getMaker(player));
	}
	
	public FurnitureMaker startMaker(Player player, Location location, String name) {
		return this.startMaker(player, location, name, new ObjectID(name, FurnitureMakerPlugin.getInstance().getName(),location));
	}
	
	public FurnitureMaker startMaker(Player player, Location location, String name, ObjectID objID) {
		if(!isInsideMaker(player)) {
			FurnitureMaker maker = new FurnitureMaker(player, location, name, objID);
			maker.getArea().show(player);
		    maker.giveSide(0, null);
		    this.furnitureMakerSet.add(maker);
		    return maker;
		}
		return null;
	}
	
	public void stopMaker(Player player) {
		FurnitureMaker maker = getMaker(player);
		if(Objects.nonNull(maker)) {
			maker.getBlockList().stream().forEach(block -> block.setType(Material.AIR));
			maker.stop();
			this.furnitureMakerSet.remove(maker);
		}
	}
}
