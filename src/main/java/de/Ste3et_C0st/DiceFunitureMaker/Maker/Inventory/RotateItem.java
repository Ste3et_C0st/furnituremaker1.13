package de.Ste3et_C0st.DiceFunitureMaker.Maker.Inventory;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.Type.BodyPart;
import de.Ste3et_C0st.FurnitureLib.main.entity.fArmorStand;
import de.Ste3et_C0st.FurnitureLib.main.entity.fDisplay;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fInteraction;

public class RotateItem extends ItemFunction{

	private final ItemAction action;
	private RotationType type = RotationType.ENTITY;
	private String[] sy = {"X", "Y", "Z"};
	private float[] dlist = {90,50,45,30,15,10,5,1};
	private int angle = 0;
	
	public RotateItem() {
		this.action = new ItemAction(ItemStackBuilder.of(Material.valueOf(FurnitureLib.isNewVersion() ? "CLOCK" : "WATCH")).setName("Rotate ArmorStand"), action -> {
			final Class<? extends fEntity> clazz = action.getMaker().getSelectedEntityType();
			
			switch (action.getType()) {
				case SWAP_LEFT:
					action.cancleEvent();
					break;
				case SWAP_RIGHT:
					action.cancleEvent();
					break;
				default: break;
			}
		});
	}
	
	@Override
	public ItemAction getItemAction() {
		return action;
	}
	
	@SuppressWarnings("unchecked")
	public enum RotationType {
		ENTITY(fDisplay.class, fArmorStand.class, fInteraction.class),
        HEAD(fArmorStand.class),
        BODY(fArmorStand.class),
        LEFT_ARM(fArmorStand.class),
        RIGHT_ARM(fArmorStand.class),
        LEFT_LEG(fArmorStand.class),
        RIGHT_LEG(fArmorStand.class);
		
		private final Class<? extends fEntity>[] classes;
		
		RotationType(Class<? extends fEntity> ... classes) {
			this.classes = classes;
		}

		public Class<? extends fEntity>[] getClasses() {
			return classes;
		}
		
		public BodyPart toBodyPart() {
			switch (this) {
				case HEAD: return BodyPart.HEAD;
				case BODY: return BodyPart.BODY;
				case LEFT_ARM: return BodyPart.LEFT_ARM;
				case RIGHT_ARM: return BodyPart.RIGHT_ARM;
				case LEFT_LEG: return BodyPart.LEFT_LEG;
				case RIGHT_LEG: return BodyPart.RIGHT_LEG;
				default: return null;
			}
		}
	}
	
	private void rotate(double angle, Player player) {
		this.getItemAction().getMaker(player).getEntityList().stream().forEach(entry -> {
			Location loc = entry.getLocation();
			loc.setYaw((float) (loc.getYaw() + angle));
			entry.teleport(loc.add(0, 1, 0));
		});
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(getFurnitureLib(), () -> {
			this.getItemAction().getMaker(player).getEntityList().stream().forEach(entry -> entry.teleport(entry.getLocation().subtract(0, 1, 0)));
		}, 2L);
	}
	
	private void rotatePart(boolean add) {
//		String coord = this.sy[this.coord];
//		double rad = dlist[this.angle] * Math.PI / 180;
//		double x = coord.equalsIgnoreCase("X") ? rad : 0;
//		double y = coord.equalsIgnoreCase("Y") ? rad : 0;
//		double z = coord.equalsIgnoreCase("Z") ? rad : 0;
//		this.getEntityList().stream().filter(fArmorStand.class::isInstance).forEach(entity -> {
//			if(add) {
//				fArmorStand stand = fArmorStand.class.cast(entity);
//				EulerAngle angle = stand.getPose(part).add(x, y, z);
//				stand.setPose(angle, part);
//			}else {
//				fArmorStand stand = fArmorStand.class.cast(entity);
//				EulerAngle angle = stand.getPose(part).subtract(x, y, z);
//				stand.setPose(angle, part);
//			}
//			entity.update();
//		});
	}

	@Override
	public String getName() {
		return "rotateEntity";
	}

	@Override
	public ItemFunction clone(Player player) {
		return new RotateItem().setPlayer(player);
	}
	
}
