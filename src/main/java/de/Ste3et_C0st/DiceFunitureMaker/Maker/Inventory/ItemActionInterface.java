package de.Ste3et_C0st.DiceFunitureMaker.Maker.Inventory;

import org.bukkit.inventory.ItemStack;

public interface ItemActionInterface {

	public ItemAction getItemAction();
	public ItemStack getStack();
	public String getName();
	public void update();
	public void tick();
	
}
