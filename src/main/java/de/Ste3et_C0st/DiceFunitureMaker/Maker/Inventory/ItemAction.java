package de.Ste3et_C0st.DiceFunitureMaker.Maker.Inventory;


import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;

public class ItemAction {

	private final ItemStack stack;
	private final Consumer<Action> action;
	
	public ItemAction(final ItemStackBuilder builder, Consumer<Action> action) {
		this(builder.build(), action);
	}
	
	public ItemAction(final ItemStack stack, Consumer<Action> action) {
		this.stack = stack;
		this.action = action;
	}
	
	public Consumer<Action> getAction(){
		return this.action;
	}
	
	public ItemStack getStack() {
		return stack;
	}
	
	public FurnitureMaker getMaker(Player player) {
		return FurnitureMakerPlugin.getInstance().getManager().getMaker(player);
	}

	public class Action{
		
		public enum ActionType{
			DROP,
			RIGHT_CLICK,
			LEFT_CLICK,
			SWAP_LEFT,
			SWAP_RIGHT
		}
		
		private final ActionType type;
		private final Player actor;
		private final Event event;
		
		public Action(ActionType type, Player actor, Event event) {
			this.type = type;
			this.actor = actor;
			this.event = event;
		}

		public ActionType getType() {
			return type;
		}

		public Event getEvent() {
			return this.event;
		}
		
		public Player getActor() {
			return actor;
		}
		
		public Player getPlayer() {
			return getActor();
		}
		
		public FurnitureMaker getMaker() {
			return FurnitureMakerPlugin.getInstance().getManager().getMaker(getActor());
		}
		
		public void cancleEvent() {
			if(event.getClass().isInstance(Cancellable.class)) {
				Cancellable.class.cast(event).setCancelled(true);
			}
		}
	}
	
}
