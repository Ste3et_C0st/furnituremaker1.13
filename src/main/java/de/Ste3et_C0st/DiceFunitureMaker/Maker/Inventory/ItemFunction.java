package de.Ste3et_C0st.DiceFunitureMaker.Maker.Inventory;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;

public abstract class ItemFunction implements ItemActionInterface{
	
	private Player player = null;
	
	public ItemStack getStack() {
		return this.getItemAction().getStack();
	}
	
	protected FurnitureLib getFurnitureLib() {
		return FurnitureLib.getInstance();
	}
	
	public boolean isStack(ItemStack stack) {
		if(stack.hasItemMeta()) {
			if(stack.getItemMeta().hasLore()) return false;
			return stack.getItemMeta().getLore().stream().filter(entry -> entry.equalsIgnoreCase(getName())).findFirst().isPresent();
		}
		return false;
	}
	
	@Override
	public void update() {
		
	}
	
	@Override
	public void tick() {
		
	}
	
	public abstract ItemFunction clone(Player player);
	
	public ItemFunction setPlayer(Player player) {
		this.player = player;
		return this;
	}
	
	public Player getPlayer() {
		return this.player;
	}
	
}
