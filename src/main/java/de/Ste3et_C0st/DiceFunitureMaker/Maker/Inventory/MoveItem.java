package de.Ste3et_C0st.DiceFunitureMaker.Maker.Inventory;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.Utilitis.LocationUtil;
import de.Ste3et_C0st.FurnitureLib.Utilitis.Relative;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class MoveItem extends ItemFunction{

	private final double[] dList = {2d,1.75,1.5,1.25,1d,.75,.5,.25,.125, .0625, .03125};
	private int distance = 4;
	private final ItemAction action;
	
	public MoveItem() {
		this.action = new ItemAction(new ItemStackBuilder(Material.PAPER).setName("Move ArmorStand"), action -> {
			final BlockFace face = LocationUtil.yawToFace(action.getPlayer().getLocation().getYaw()).getOppositeFace();
			final double distance = this.dList[this.distance];
			switch (action.getType()) {
				case SWAP_LEFT: 
					this.teleport(face, distance, true, action.getMaker());
					action.cancleEvent();
					break;
				case SWAP_RIGHT:
					this.teleport(face, distance, false, action.getMaker());
					action.cancleEvent();
					break;
				case LEFT_CLICK:
					if(this.distance > 0) {this.distance--;sendMoveMessage(action.getActor());}
					break;
				case RIGHT_CLICK:
					if(this.distance < (this.dList.length-1)) {this.distance++;sendMoveMessage(action.getActor());}
					break;
				default: break;
			}
		});
	}
	
	@Override
	public ItemAction getItemAction() {
		return this.action;
	}

	@Override
	public String getName() {
		return "moveItem";
	}
	
	@Override
	public ItemFunction clone(Player player) {
		return new RotateItem().setPlayer(player);
	}
	
	private void sendMoveMessage(final Player player) {
		player.spigot().sendMessage(new ComponentBuilder("§bMove size changed to:§e " + this.dList[this.distance]).create());
		player.playSound(player.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1.0F, (float)dList[this.distance]);
	}
	
	public void teleport(BlockFace face,final double distance,boolean forward, FurnitureMaker maker) {
		final float pitch = maker.getPlayer().getLocation().getPitch();
		for(fEntity stand : maker.getEntityList()){
			Relative relative = null;
			if(pitch<-70){
				relative = new Relative(stand.getLocation().clone(), 0,(forward ? -distance : distance), 0, face);	
			}else if(pitch>70) {
				relative = new Relative(stand.getLocation().clone(), 0,(forward ? distance : -distance), 0, face);
			}else {
				relative = new Relative(stand.getLocation().clone(), distance, 0, 0, face);
			}
			
			if(Objects.nonNull(relative)) {
				Location l = relative.getSecondLocation();
				if(maker.getArea().isInside(l)) {
					l.setYaw(stand.getLocation().getYaw());
					l.setPitch(stand.getLocation().getPitch());
					stand.teleport(l);
				}
			}
		}
	}
}
