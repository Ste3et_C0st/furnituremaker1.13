package de.Ste3et_C0st.DiceFunitureMaker.Maker;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.EulerAngle;

import com.comphenix.protocol.utility.MinecraftVersion;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.ArmorStandInventory;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.ArmorStandMetadata;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.ArmorStandSelector;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.FurnitureMakerInventory.FurnitureDisplayInventory;
import de.Ste3et_C0st.FurnitureLib.ModelLoader.ModelFileLoader;
import de.Ste3et_C0st.FurnitureLib.Utilitis.DoubleKey;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.Utilitis.LocationUtil;
import de.Ste3et_C0st.FurnitureLib.Utilitis.Relative;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureManager;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.BodyPart;
import de.Ste3et_C0st.FurnitureLib.main.Type.PlaceableSide;
import de.Ste3et_C0st.FurnitureLib.main.Type.SQLAction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fArmorStand;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.monster.fMagmaCube;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class FurnitureMakerInventory extends FurnitureMakerSerializer{

	public HashMap<DoubleKey<Integer>,ItemStack> stackList = new HashMap<DoubleKey<Integer>,ItemStack>();
	private int inventory = 0;
	private String[] sy = {"X", "Y", "Z"};
	private double[] dList = {2d,1.75,1.5,1.25,1d,.75,.5,.25,.125, .0625, .03125};
	private float[] dlist = {90,50,45,30,15,10,5,1};
	private int coord = 0, distance = 4, angle = 0, bodypartValue = 0, placeAbleSideValue = 0;
	private FurnitureDisplayInventory furnitureDisplay = null;
	private final BukkitTask task;
	
	protected final ItemStack[] content, armor;
	protected final GameMode oldMode;
	
	private Player player;
	private ItemStack[] slots = null;
	private int side = 0;
	
	private Supplier<String> degresseSupplier = () -> "§7Rotate ArmorStand §4" + dlist[this.angle] + "°";
	private Supplier<String> bodyPartName = () -> "§3" + BodyPart.values()[this.bodypartValue] + " §7Rotation [§c" + sy[this.coord] + ":" + dlist[this.angle] + "°§7]";
	private final Material clockMaterial = Material.valueOf(FurnitureLib.isNewVersion() ? "CLOCK" : "WATCH");
	
	public FurnitureMakerInventory(Player player, Location location, String modelName, ObjectID objID) {
		super(player, location, modelName, objID);
		this.player = player;
		this.content = player.getInventory().getContents();
		this.armor = player.getInventory().getArmorContents();
		this.oldMode = player.getGameMode();
		
		//first hotbar
		stackList.put(new DoubleKey<Integer>(0, 0), new ItemStackBuilder(Material.ARMOR_STAND).setName("§9┤§6" + modelName + "§9├ Add ArmorStand").build());
		stackList.put(new DoubleKey<Integer>(0, 8), new ItemStackBuilder(Material.BARRIER).setName("§cAbort").build());
		
		//secound hotbar <ArmorStand>
		stackList.put(new DoubleKey<Integer>(1, 0), new ItemStackBuilder(Material.ARMOR_STAND).setName("§9Spawn ArmorStand").build());
		stackList.put(new DoubleKey<Integer>(1, 1), new ItemStackBuilder(Material.PAPER).setName("§3Move Entity").setLore(
				"§7Sneek + Scroll: §3Move entities",
				"§8§m----------------------------------",
				"§7RightClick: §cdecrease §7move size", 
				"§7LeftClick: §2increase §7move size").build());
		
		stackList.put(new DoubleKey<Integer>(1, 2), new ItemStackBuilder(Material.FEATHER).setName("Edit ArmorStand").build());
		stackList.put(new DoubleKey<Integer>(1, 3), new ItemStackBuilder(clockMaterial).setLore(
				"§7Sneek + Scroll: §3Rotate ArmorStand",
				"§8§m----------------------------------",
				"§7RightClick: §cdecrease §7rotate degreese", 
				"§7LeftClick: §2increase §7rotate degreese"
				)
				.setName(degresseSupplier.get()).build());
		stackList.put(new DoubleKey<Integer>(1, 4), new ItemStackBuilder(Material.CHEST).setName("Edit Inventory").build());
		stackList.put(new DoubleKey<Integer>(1, 5), new ItemStackBuilder(Material.BLAZE_ROD).setName(bodyPartName.get())
				.setLore(
						"§7Sneek + Scroll: §3Change BodyPart Rotation",
						"§8§m----------------------------------",
						"§7RightClick: change working area (§dHead§7|§dBody§7|§dArms§7|§dLegs§7)",
						"§7LeftClick: change working area (§dHead§7|§dBody§7|§dArms§7|§dLegs§7)",
						"§7Sneek + RightClick: §cdecrease §eworking size",
						"§7Sneek + LeftClick: §2increase §eworking size",
						"§8§m----------------------------------",
						"§7Use the rotation of §nRotation Tool"
						)
				.build());
		stackList.put(new DoubleKey<Integer>(1, 7), new ItemStackBuilder(Material.ENDER_CHEST).setName("§3Select Entities").build());
		stackList.put(new DoubleKey<Integer>(1, 8), new ItemStackBuilder(Material.STICK).setName("§6◄ Back").build());
		//secound hotbar <Display>
		
		//finish page
		stackList.put(new DoubleKey<Integer>(2, 0), new ItemStackBuilder(Material.STICK).setName("§6► Editor").build());
		stackList.put(new DoubleKey<Integer>(2, 1), new ItemStackBuilder(Material.BARRIER).setName("§cAbort").build());
		stackList.put(new DoubleKey<Integer>(2, 2), new ItemStackBuilder(Material.BLAZE_POWDER).setName("Rotate all selected armorstands").build());
		stackList.put(new DoubleKey<Integer>(2, 3), new ItemStackBuilder(Material.valueOf(FurnitureLib.isNewVersion() ? "GREEN_BANNER" : "BANNER")).setName(getPlaceAbleSide()).build());
		stackList.put(new DoubleKey<Integer>(2, 4), new ItemStackBuilder(Material.PAPER).setName("Move all entities").setLore(
				"§7Sneek + Scroll: §3Move entities",
				"§8§m----------------------------------",
				"§7RightClick: §cdecrease §7move size", 
				"§7LeftClick: §2increase §7move size").build());
		stackList.put(new DoubleKey<Integer>(2, 8), new ItemStackBuilder(Material.DIAMOND).setName("§6FINISH").build());
		
		if(FurnitureLib.getVersion(new MinecraftVersion("1.19.4"))) {
			this.furnitureDisplay = new FurnitureDisplayInventory(player, this);
		}
		
		if(FurnitureManager.getInstance().isEntityTypeRegistred(EntityType.MAGMA_CUBE)) {
			stackList.put(new DoubleKey<Integer>(7, 0), ItemStackBuilder.of(Material.MAGMA_CREAM).setName("§9Spawn Hitbox").build());
			stackList.put(new DoubleKey<Integer>(7, 1), getItem(1, 1).get());
			stackList.put(new DoubleKey<Integer>(7, 7), getItem(1, 7).get());
			stackList.put(new DoubleKey<Integer>(7, 8), getItem(1, 8).get());
		}
		
		this.getObjectID().getPlayerList().add(player);
		this.giveSide(this.inventory, null);
		getPlayer().setGameMode(GameMode.CREATIVE);
		
		this.task = Bukkit.getScheduler().runTaskTimer(getFurnitureLib(), () -> {
			this.onTick();
		}, 20, 20);
	}
	
	public boolean contains(ItemStack stack) {
		return this.contains(this.inventory, stack);
	}
	
	public boolean contains(int side, ItemStack stack) {
		return stackList.entrySet().stream().filter(entry -> entry.getKey().getKey1() == side && stack.equals(entry.getValue())).findFirst().isPresent();
	}
	
	public void giveSide(int i, Class<? extends fEntity> entitySelector) {
		this.saveItems();
		this.setEntityType(entitySelector);
		
		if(i == 0 && this.getObjectID().getPacketList().size() > 0) {
			i = 2;
			this.select(this.getObjectID().getPacketList());
		}else if(i > 0) {
			this.selectFirstClass(entitySelector);
		}
		
		for(int j = 0; j < 9; j++) {
			this.player.getInventory().clear(j);
		}
		
		this.side = i;
		this.stackList.entrySet().stream().filter(entry -> entry.getKey().getKey1().equals(this.side)).forEach(entry -> {
			this.player.getInventory().setItem(entry.getKey().getKey2(), entry.getValue());
		});
	}
	
	private void onTick() {
		if(this.furnitureDisplay != null) this.furnitureDisplay.onTick();
	}
	
	public Optional<ItemStack> getItem(int side, int slot) {
		return this.stackList.entrySet().stream().filter(entry -> entry.getKey().getKey1() == side).filter(entry -> entry.getKey().getKey2() == slot).map(Entry::getValue).findFirst();
	}
	
	private void saveItems() {
		if(Objects.isNull(this.slots)) {
			this.slots = new ItemStack[10];
			for(int hotbarslot = 0; hotbarslot < 9; hotbarslot++) {
				if(Objects.nonNull(this.player.getInventory().getItem(hotbarslot))){
					this.slots[hotbarslot] = this.player.getInventory().getItem(hotbarslot);
					this.player.getInventory().setItem(hotbarslot, new ItemStack(Material.AIR));
				}
			}
		}
	}
	
	public String getPlaceAbleSide(){
		String returnStr = "§cBuild-Block Position:";
		String side = "§e Top Of Block";
		
		switch (getPlaceableSide()) {
			case BOTTOM:side = "§e Bottom Of Block";break;
			case TOP:break;
			case SIDE:side = "§e Side Of Block";break;
			default:break;
		}
		
		return returnStr + side;
	}
	
	public Player getPlayer() {
		return this.player;
	}
	
	public boolean onItemSlotForward(ItemStack stack) {
		if(stack.getType() == Material.PAPER) {
			BlockFace face = FurnitureMakerPlugin.getInstance().getFurnitureLib().getLocationUtil().yawToFace(getPlayer().getLocation().getYaw()).getOppositeFace();
			double distance = this.dList[this.distance];
			this.teleport(face, distance, getPlayer().getLocation().getPitch(), true);
			return true;
		}else if(stack.getType() == this.clockMaterial) {
			this.rotate(this.dlist[this.angle]);
			return true;
		}else if(stack.getType() == Material.BLAZE_ROD) {
			this.rotatePart(true);
			this.rebuildStack(stack, bodyPartName);
			return true;
		}else if(stack.getType() == Material.BLAZE_POWDER) {
			rotateAll(true);
			return true;
		}else if(Objects.nonNull(this.furnitureDisplay)){
			return this.furnitureDisplay.onItemSlotForward(stack);
		}
		return false;
	}
	
	public List<Block> getBlockList() {
		return blockList;
	}

	public boolean onItemSlotBackward(ItemStack stack) {
		if(stack.getType() == Material.PAPER) {
			BlockFace face = FurnitureMakerPlugin.getInstance().getFurnitureLib().getLocationUtil().yawToFace(getPlayer().getLocation().getYaw());
			double distance = this.dList[this.distance];
			this.teleport(face, distance, getPlayer().getLocation().getPitch(), false);
			return true;
		}else if(stack.getType() == this.clockMaterial) {
			this.rotate(-(this.dlist[this.angle]));
			return true;
		}else if(stack.getType() == Material.BLAZE_ROD) {
			this.rotatePart(false);
			this.rebuildStack(stack, bodyPartName);
			return true;
		}else if(stack.getType() == Material.BLAZE_POWDER) {
			rotateAll(false);
			return true;
		}else if(Objects.nonNull(this.furnitureDisplay)){
			return this.furnitureDisplay.onItemSlotBackward(stack);
		}
		return false;
	}
	
	public void teleport(BlockFace face,final double distance, float pitch,boolean forward) {
		for(fEntity stand : this.getEntityList()){
			if(stand == null) continue;
			final Location workingLocation = stand.getLocation().clone();
			Relative relative = null;
			if(pitch<-70){
				relative = new Relative(workingLocation, 0,(forward ? -distance : distance), 0, face);	
			}else if(pitch>70) {
				relative = new Relative(workingLocation, 0,(forward ? distance : -distance), 0, face);
			}else {
				relative = new Relative(workingLocation, distance, 0, 0, face);
			}
			
			if(Objects.nonNull(relative)) {
				Location l = relative.getSecondLocation();
				if(getArea().isInside(l)) {
					l.setYaw(stand.getLocation().getYaw());
					l.setPitch(stand.getLocation().getPitch());
					stand.teleport(l);
				}
			}
		}
	}
	
	public void rotate(double angle) {
		this.getEntityList().stream().forEach(entry -> {
			Location loc = entry.getLocation().clone();
			loc.setYaw((float) (loc.getYaw() + angle));
			entry.teleport(loc.add(0, 1, 0));
		});
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(getFurnitureLib(), () -> {
			this.getEntityList().stream().forEach(entry -> entry.teleport(entry.getLocation().subtract(0, 1, 0)));
		}, 2L);
	}

	public boolean onItemStackRightClick(ItemStack stack) {
		if(stack.getType() == Material.MAGMA_CREAM) {
			FurnitureMakerPlugin.getInstance().getManager().getMaker(player).addEntity(fMagmaCube.class);
			return true;
		}else if(stack.getType() == Material.ARMOR_STAND) {
			if(this.side == 0 || this.side == 2) {
				this.giveSide(1, fArmorStand.class);
				this.addEntity();
			}else {
				this.addEntity();
			}
			return true;
		}else if(stack.getType() == Material.PAPER) {
			if(this.distance < (this.dList.length-1)) {this.distance++;sendMoveMessage();}
			return true;
		}else if(stack.getType().equals(clockMaterial)) {
			if(this.angle < (this.dlist.length-1)) {
				this.angle++;
				sendRotateMessage();
				this.rebuildStack(stack, degresseSupplier);
				this.rebuildStack(Material.BLAZE_ROD,player.getInventory().getItem(5), bodyPartName);
			}
			return true;
		}else if(stack.getType() == Material.FEATHER) {
			new ArmorStandMetadata((fArmorStand) this.getEntityList().stream().findFirst().orElse(null), getPlayer(), getObjectID());
			return true;
		}else if(stack.getType() == Material.CHEST) {
			new ArmorStandInventory((fArmorStand) this.getEntityList().stream().findFirst().orElse(null), getPlayer());
			return true;
		}else if(stack.getType() == Material.BLAZE_ROD) {
			if(!getPlayer().isSneaking()) {
				if(this.bodypartValue < (BodyPart.values().length-1)) {this.bodypartValue++;}
			}else {
				if(this.coord < (this.sy.length -1)) this.coord++;
			}
			this.rebuildStack(stack, bodyPartName);
			return true;
		}else if(stack.getType() == Material.ENDER_CHEST) {
			new ArmorStandSelector(this, entitySelectorClass);
			return true;
		}else if(stack.getType() == Material.STICK) {
			if(this.side > 0) {
				this.giveSide(0, null);
			}
			return true;
		}else if(stack.getType() == Material.BARRIER) {
			List<fEntity> entityList = new ArrayList<fEntity>(this.getEntityList());
			this.removeEntities(entityList);
			FurnitureMakerPlugin.getInstance().getManager().stopMaker(getPlayer());
			return true;
		}else if(stack.getType() == Material.DIAMOND) {
			File file = this.save(this.getPlayer().getUniqueId(), this.placeAbleSide);
			FurnitureMakerPlugin.getInstance().getManager().stopMaker(getPlayer());
			List<fEntity> entityList = new ArrayList<fEntity>(this.getEntityList());
			this.removeEntities(entityList);
			if(Objects.nonNull(file)) ModelFileLoader.loadModelFile(file);
		}else if(stack.getType().name().contains("BANNER")) {
			if(this.placeAbleSideValue < (PlaceableSide.values().length-1)) {
				this.placeAbleSideValue++;
				this.placeAbleSide = PlaceableSide.values()[this.placeAbleSideValue];
				ItemMeta meta = stack.getItemMeta();
				meta.setDisplayName(getPlaceAbleSide());
				stack.setItemMeta(meta);
				return true;
			}
		}else if(Objects.nonNull(this.furnitureDisplay)){
			return this.furnitureDisplay.onItemStackRightClick(stack);
		}
		return false;
	}

	public boolean onItemStackLeftClick(ItemStack stack) {
		if(stack.getType() == Material.ARMOR_STAND) {
			if(this.side == 0) {
				this.giveSide(1, fArmorStand.class);
				this.addEntity();
			}else {
				this.addEntity();
			}
			return true;
		}else if(stack.getType() == Material.PAPER) {
			if(this.distance > 0) {this.distance--;sendMoveMessage();}
			return true;
		}else if(stack.getType().equals(clockMaterial)){
			if(this.angle > 0) {
				this.angle--;
			    sendRotateMessage();
			    this.rebuildStack(stack, degresseSupplier);
			    this.rebuildStack(Material.BLAZE_ROD,player.getInventory().getItem(5), bodyPartName);
			}
			return true;
		}else if(stack.getType() == Material.BLAZE_ROD){
			if(!getPlayer().isSneaking()) {
				if(this.bodypartValue > 0) {this.bodypartValue--;}
			}else {
				if(this.coord > 0) this.coord--;
			}
			this.rebuildStack(stack, bodyPartName);
			return true;
		}else if(stack.getType() == Material.STICK) {
			if(this.side == 0) {
				this.giveSide(1, fArmorStand.class);
			}else {
				this.giveSide(0, null);
			}
		}else if(stack.getType().name().contains("BANNER")) {
			if(this.placeAbleSideValue > 0) {
				this.placeAbleSideValue--;
				this.placeAbleSide = PlaceableSide.values()[this.placeAbleSideValue];
				ItemMeta meta = stack.getItemMeta();
				meta.setDisplayName(getPlaceAbleSide());
				stack.setItemMeta(meta);
				return true;
			}
		}else if(Objects.nonNull(this.furnitureDisplay)){
			return this.furnitureDisplay.onItemStackLeftClick(stack);
		}
		return false;
	}

	public boolean onItemPointer(ItemStack stack) {
		if(Objects.nonNull(this.furnitureDisplay)){
			return this.furnitureDisplay.onItemPointer(stack);
		}
		return false;
	}

	public boolean onItemDrop(ItemStack stack) {
		if(Objects.nonNull(this.furnitureDisplay)){
			return this.furnitureDisplay.onItemDrop(stack);
		}
		return false;
	}
	
	public boolean isItem(ItemStack stack) {
		return Objects.nonNull(this.stackList.values().stream().filter(is -> is.equals(stack)).findFirst().orElse(null));
	}
	
	private void rotatePart(boolean add) {
		String coord = this.sy[this.coord];
		double rad = dlist[this.angle] * Math.PI / 180;
		double x = coord.equalsIgnoreCase("X") ? rad : 0;
		double y = coord.equalsIgnoreCase("Y") ? rad : 0;
		double z = coord.equalsIgnoreCase("Z") ? rad : 0;
		BodyPart part = BodyPart.values()[this.bodypartValue];
		this.getEntityList().stream().filter(fArmorStand.class::isInstance).forEach(entity -> {
			if(add) {
				fArmorStand stand = fArmorStand.class.cast(entity);
				EulerAngle angle = stand.getPose(part).add(x, y, z);
				stand.setPose(angle, part);
			}else {
				fArmorStand stand = fArmorStand.class.cast(entity);
				EulerAngle angle = stand.getPose(part).subtract(x, y, z);
				stand.setPose(angle, part);
			}
			entity.update();
		});
	}
	
	private void rotateAll(boolean add) {
		Location center = getObjectID().getStartLocation().clone().add(.5, 0, .5);
		BlockFace face = add ? BlockFace.WEST : BlockFace.EAST;
		LocationUtil util = FurnitureLib.getInstance().getLocationUtil();
		this.getObjectID().getPacketList().stream().forEach(entity -> {
		    	Location armorStandLocation = entity.getLocation();
		    	float yaw = armorStandLocation.getYaw() + (add ? -90 : 90);
		    	Relative distance = new Relative(center.clone(), armorStandLocation);
		    	Location newLocation = util.getRelative(center.clone(), face, -distance.getOffsetZ(), -distance.getOffsetX());
		    	newLocation.setYaw(yaw);
		    	newLocation.setPitch(armorStandLocation.getPitch());
		    	newLocation.setY(armorStandLocation.getY());
		    	entity.teleport(newLocation);
		    });
		    
		getObjectID().setSQLAction(SQLAction.NOTHING);
	}
	
	private void sendMoveMessage() {
		this.player.spigot().sendMessage(new ComponentBuilder("§bMove size changed to:§e " + this.dList[this.distance]).create());
		this.player.playSound(this.player.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1.0F, (float)dList[this.distance]);
	}
	
	private void sendRotateMessage() {
		this.player.spigot().sendMessage(new ComponentBuilder("§bRotate size changed to:§e " + this.dlist[this.angle] + "§b°").create());
		this.player.playSound(this.player.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1.0F, (float)dlist[this.angle]);
	}
	
	
	public void rebuildStack(ItemStack originalStack, Supplier<String> name) {
		rebuildStack(originalStack.getType(), originalStack, name);
	}
	
	public void rebuildStack(Material material, ItemStack originalStack, Supplier<String> name) {
		if(originalStack==null) return;
		ItemStack stack = getStack(originalStack.getType());
		if(stack==null) return;
		if(material != stack.getType()) return;
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name.get());
		stack.setItemMeta(meta);
		originalStack.setItemMeta(meta);
		getPlayer().updateInventory();
	}
	
	private ItemStack getStack(Material material) {
		return this.stackList.values().stream().filter(a -> a.getType() == material).findFirst().orElse(null);
	}
	
	public void stop() {
		getPlayer().setGameMode(this.oldMode);
		getPlayer().getInventory().setContents(this.content);
		getPlayer().getInventory().setArmorContents(this.armor);
		this.task.cancel();
		super.stop();
	}
	
	public HashMap<DoubleKey<Integer>,ItemStack> getInventoryMap(){
		return this.stackList;
	}
	
	public long getEntitySize(Class<? extends fEntity> entityClazz) {
		return getObjectID().getPacketList().stream().filter(entry -> entry.getClass().equals(entityClazz)).count();
	}
}
