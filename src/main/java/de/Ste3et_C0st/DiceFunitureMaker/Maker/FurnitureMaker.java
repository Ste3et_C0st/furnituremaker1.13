package de.Ste3et_C0st.DiceFunitureMaker.Maker;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.SQLAction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class FurnitureMaker extends FurnitureMakerInventory{
	
	public FurnitureMaker(Player player, Location location, String modelName, ObjectID objID) {
		super(player, location, modelName, objID);
		this.sendStartMessage(player);
	}
	
	private void sendStartMessage(Player player) {
		player.sendMessage("§n§2Short Description:");
		player.sendMessage("§cRed Wool Block: " + "§7is the start position");
		player.sendMessage("§2Green Wool Block: " + "§7is the North Direction");
		player.sendMessage("§9Blue Wool Block: " + "§7is the East Direction");
		player.sendMessage("§fWhite Wool Block: " + "§7is the perfect working area");
		player.spigot().sendMessage(new ComponentBuilder("§6- if you need help visit this ").append("§n§2side").event(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://dicecraft.de/furniture/tutorial.php")).create()); 
	}
	
	public void copyObjectID(ObjectID objectID) {
		getObjectID().getPacketList().forEach(entity -> {
			fEntity clone = entity.clone();
			clone.setLocation(entity.getLocation());
			getObjectID().addArmorStand(clone);
		});
		
		objectID.getBlockList().stream().forEach(location -> {
			Block block = location.getBlock();
			this.blockList.add(block);
		});
		
		getObjectID().getPacketList().stream().forEach(entity -> entity.send(this.getPlayer()));
		this.select(getObjectID().getPacketList());
		objectID.setPrivate(true);
		objectID.removeAll();
		objectID.getBlockList().clear();
		objectID.setSQLAction(SQLAction.REMOVE);
		objectID.getPacketList().clear();
		objectID.getPlayerList().clear();
		objectID.setFunctionObject(null);
	}
}
