package de.Ste3et_C0st.DiceFunitureMaker.Maker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WorldBorder;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.WorldBorderAction;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.AquaBlock;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.CombatBlock;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.PacketBlock;
import de.Ste3et_C0st.FurnitureLib.Utilitis.BoundingBox;
import de.Ste3et_C0st.FurnitureLib.Utilitis.Relative;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;

public class FurnitureMakerArea {
	
	private List<PacketBlock> packetBlockList = new ArrayList<PacketBlock>();
	
	private Location center, corner1, corner2, executeBlock;
	private BoundingBox boundingbox;
	private int size;
	
	public FurnitureMakerArea(Location center, int size) {
		this.center = center;
		this.size = size;
		this.corner1 = FurnitureLib.getInstance().getLocationUtil().getRelative(this.center, BlockFace.EAST, size-1, size);
		this.corner2 = FurnitureLib.getInstance().getLocationUtil().getRelative(this.center, BlockFace.EAST, -size, -size+1);
		this.corner1.setY(FurnitureLib.getVersionInt() > 17 ? center.getWorld().getMinHeight() : 1);
		this.corner2.setY(center.getWorld().getMaxHeight());
		this.boundingbox = BoundingBox.of(this.corner1, this.corner2);
		
		for(int x = 0; x < size ; x++){
			for(int y = 0; y < size ; y++){
				Location loc = new Relative(center, -x, -1, y, BlockFace.NORTH).getSecondLocation();
				if(FurnitureLib.isNewVersion()) {
					this.packetBlockList.add(new AquaBlock(loc, Material.WHITE_WOOL));
				}else {
					this.packetBlockList.add(new CombatBlock(loc, Material.valueOf("WOOL"), (short) 0));
				}
			}
		}
		
		this.executeBlock = this.center.getBlock().getLocation().add(0, 1, 0);
		
		if(FurnitureLib.isNewVersion()) {
			this.packetBlockList.add(new AquaBlock(new Relative(this.center, -1, -1, 0, BlockFace.NORTH).getSecondLocation(), Material.RED_WOOL));
			this.packetBlockList.add(new AquaBlock(new Relative(this.center, 0, -1, 0, BlockFace.NORTH).getSecondLocation(), Material.GREEN_WOOL));
			this.packetBlockList.add(new AquaBlock(new Relative(this.center, 0, -1, 1, BlockFace.NORTH).getSecondLocation(), Material.BLUE_WOOL));
		}else {
			this.packetBlockList.add(new CombatBlock(new Relative(this.center, 0, -1, 0, BlockFace.NORTH).getSecondLocation(), Material.valueOf("WOOL"), (short) 14));
			this.packetBlockList.add(new CombatBlock(new Relative(this.center, -1, -1, 0, BlockFace.NORTH).getSecondLocation(),  Material.valueOf("WOOL"), (short) 13));
			this.packetBlockList.add(new CombatBlock(new Relative(this.center, 0, -1, 1, BlockFace.NORTH).getSecondLocation(),  Material.valueOf("WOOL"), (short) 11));
		}
	}
	
	public void show(Player player) {
		packetBlockList.forEach(packet -> packet.parseBlock(player));
		sendWorldBorder(player);
	}
	
	public void reset(Player player) {
		packetBlockList.forEach(packet -> packet.resetBlock(player));
		resetWorldBorder(player);
	}
	
	private void sendWorldBorder(Player player) {
		PacketContainer border;
		if(FurnitureLib.getVersionInt() > 16) {
			border = new PacketContainer(PacketType.Play.Server.INITIALIZE_BORDER);
		}else {
			border = new PacketContainer(PacketType.Play.Server.WORLD_BORDER);
		}
		
		border.getWorldBorderActions().write(0, WorldBorderAction.INITIALIZE);
		border.getIntegers()
			.write(0, 29999984)
			.write(1, 0)
			.write(2, 0);
		border.getLongs().writeSafely(0, 0L);
		border.getDoubles()
			.write(0, FurnitureLib.getInstance().getLocationUtil().getCenter(this.center).getX())
			.write(1, FurnitureLib.getInstance().getLocationUtil().getCenter(this.center).getZ())
			.write(2, (double) size*2)
			.writeSafely(3, (double) size*2);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, border);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void resetWorldBorder(Player player) {
		if(player.getWorld().getWorldBorder()!=null){
			WorldBorder wb = player.getWorld().getWorldBorder();
			PacketContainer border;
			if(FurnitureLib.getVersionInt() > 16) {
				border = new PacketContainer(PacketType.Play.Server.SET_BORDER_SIZE);
				border.getDoubles()
					.write(0, wb.getSize());
			}else {
				border = new PacketContainer(PacketType.Play.Server.WORLD_BORDER);
				border.getWorldBorderActions().write(0, WorldBorderAction.INITIALIZE);
				border.getIntegers()
				.writeSafely(0, 29999984)
				.writeSafely(1, wb.getWarningTime())
				.writeSafely(2, wb.getWarningDistance());
				border.getLongs()
					.write(0, 0L);
				border.getDoubles()
					.write(0, wb.getCenter().getX())
					.write(1, wb.getCenter().getZ())
					.write(2, wb.getSize())
					.write(3, wb.getSize());
			}
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(player, border);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			PacketContainer border;
			if(FurnitureLib.getVersionInt() > 16) {
				border = new PacketContainer(PacketType.Play.Server.SET_BORDER_SIZE);
				border.getDoubles()
					.write(0, Double.MAX_VALUE);
			}else {
				border = new PacketContainer(PacketType.Play.Server.WORLD_BORDER);
				border.getWorldBorderActions().write(0, WorldBorderAction.INITIALIZE);
				border.getIntegers()
				.write(0, 29999984)
				.write(1, 0)
				.write(2, 0);
				border.getLongs()
					.write(0, 0L);
				border.getDoubles()
					.write(0, player.getWorld().getSpawnLocation().getX())
					.write(1, player.getWorld().getSpawnLocation().getZ())
					.write(2, Double.MAX_VALUE)
					.write(3, Double.MAX_VALUE);
			}
			
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(player, border);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean isInside(Location loc) {
		return this.boundingbox.contains(loc.toVector());
	}
	
	public String toString() {
		return Objects.isNull(this.boundingbox) ? "FurnitureMakerArea: isNULL" : "FurnitureMakerArea: " + this.boundingbox.toString();
	}

	public Location getExecuteBlock() {
		return executeBlock;
	}

	public Location getCenter() {
		return this.center;
	}
}
