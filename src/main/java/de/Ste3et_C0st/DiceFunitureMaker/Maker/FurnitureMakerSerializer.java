package de.Ste3et_C0st.DiceFunitureMaker.Maker;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Display.Billboard;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Directional;
import org.bukkit.material.MaterialData;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.CraftSkull;
import de.Ste3et_C0st.FurnitureLib.Crafting.Project;
import de.Ste3et_C0st.FurnitureLib.SchematicLoader.ProjectMetadata;
import de.Ste3et_C0st.FurnitureLib.Utilitis.Relative;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureManager;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.PlaceableSide;
import de.Ste3et_C0st.FurnitureLib.main.Type.SQLAction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fBlock_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fDisplay;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fInteraction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fItem_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fText_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.monster.fMagmaCube;

@SuppressWarnings("deprecation")
public abstract class FurnitureMakerSerializer extends ProjectMetadata{
	
	private Location location;
	private FurnitureMakerArea area;
	private String modelName;
	private ObjectID objectID;
	protected PlaceableSide placeAbleSide = PlaceableSide.TOP;
	private Player player;
	
	private HashSet<fEntity> packetList = new HashSet<fEntity>();
	protected List<Block> blockList = new ArrayList<Block>();

	public FurnitureMakerSerializer(Player player, Location location, String modelName, ObjectID objID) {
		this.objectID = objID;
		this.objectID.setPrivate(true);
		this.objectID.getPlayerList().add(player);
		this.location = location.getBlock().getLocation();
		this.location.add(.5, 0, .5);
		this.location.setYaw(0);
		this.modelName = modelName;
		this.placeAbleSide = Objects.isNull(getProject()) ? PlaceableSide.TOP : getProject().getPlaceableSide();
		this.area = new FurnitureMakerArea(getStartLocation(), 10);
		this.player = player;
	}
	
	public String getModelName() {
		return this.modelName;
	}
	
	public Location getStartLocation() {
		return this.location.clone();
	}
	
	public FurnitureMakerArea getArea() {
		return this.area;
	}
	
	public ObjectID getObjectID() {
		return this.objectID;
	}
	
	public FurnitureLib getFurnitureLib() {
		return FurnitureLib.getInstance();
	}
	
	public FurnitureManager getFurnitureManager() {
		return getFurnitureLib().getFurnitureManager();
	}
	
	public fEntity addEntity() {
		fEntity entity = FurnitureLib.getInstance().getFurnitureManager().createArmorStand(getObjectID(), getFurnitureLib().getLocationUtil().getCenter(getStartLocation()).clone().subtract(0, .5, 0));
		return addEntity(entity);
	}
	
	public fEntity addEntity(Class<? extends fEntity> entityClass) {
		BiFunction<Location, ObjectID, fEntity> function = (t, u) -> {
			try {
				return entityClass.getConstructor(Location.class, ObjectID.class).newInstance(t.clone(), u);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		};
		
		final Location spawnLocation = getFurnitureLib().getLocationUtil().getCenter(getStartLocation()).clone().subtract(0, .5, 0);
		fEntity entity = function.apply(spawnLocation, getObjectID());
		
		if(entity instanceof fBlock_display) {
			fBlock_display display = fBlock_display.class.cast(entity);
			display.setBlockData(Material.STONE);
			display.teleport(display.getLocation().subtract(.5, 0, .5));
		}
		
		if(entity instanceof fText_display display) {
			display.teleport(display.getLocation().add(0, 1, 0));
			display.setText("Hello World");
			display.setBillboard(Billboard.FIXED);
		}
		
		if(entity instanceof fInteraction display) {
			display.setResponse(false);
		}
		
		if(entity instanceof fMagmaCube shulker) {
			shulker.setInvisible(true);
			shulker.setSize(2);
		}
		
		if(entity instanceof fItem_display display) {
			display.setItemStack(new ItemStack(Material.DIAMOND_SWORD));
		}
		
		if(entity instanceof fDisplay display) {
			display.setBlockLight(15);
			display.setScale(new org.joml.Vector3f(1f,1f,1f));
		}
		
		getObjectID().addArmorStand(entity);
		
		return addEntity(entity);
	}
	
	public fEntity addEntity(fEntity entity) {
		getObjectID().setSQLAction(SQLAction.NOTHING);
		this.packetList.clear();
		this.packetList.add(entity);
		highLightEntities(entity);
		return entity;
	}
	
	public void highLight(fEntity entity) {
		if(!this.packetList.contains(entity)) {
			this.packetList.add(entity);
			highLightEntities(this.packetList.stream().toArray(fEntity[] ::new));
		}
	}
	
	protected FurnitureMaker getMaker() {
		return FurnitureMakerPlugin.getInstance().getManager().getMaker(this.player);
	}
	
	public void removeHighlight(fEntity entity) {
		if(this.packetList.contains(entity)) {
			this.packetList.remove(entity);
			highLightEntities(this.packetList.stream().toArray(fEntity[] ::new));
		}
	}
	
	public void highLightEntities(HashSet<fEntity> entities) {
		highLightEntities(entities.stream().toArray(fEntity[] ::new));
	}
	
	public void highLightEntities(fEntity ... entities) {
		if(Objects.isNull(entities)) return;
		List<fEntity> entityList = new ArrayList<fEntity>(Arrays.asList(entities));
		
		getObjectID().getPacketList().stream().forEach(entity -> {
			if(entityList.contains(entity)) {
				entity.setGlowing(true);
			}else {
				entity.setGlowing(false);
			}
		});
		
		getObjectID().getPacketList().stream().forEach(entity -> entity.send(this.player));
	}
	
	public void select(HashSet<fEntity> entityList) {
		this.select(entityList.stream().toArray(fEntity[] ::new));
	}
	
	public void selectFirst() {
		if(getObjectID().getPacketList().isEmpty()) return;
		this.select(getEntityList().stream().findFirst().orElse(getObjectID().getPacketList().stream().findFirst().orElse(null)));
	}
	
	public void selectFirstClass(Class<? extends fEntity> classs) {
		if(getObjectID().getPacketList().isEmpty()) return;
		this.select(getEntityList().stream().filter(entry -> entry.getClass().equals(classs)).findFirst().orElse(getObjectID().getPacketList().stream().filter(entry -> entry.getClass().equals(classs)).findFirst().orElse(null)));
	}
	
	public void selectAllByClass(Class<? extends fEntity> classs) {
		if(getObjectID().getPacketList().isEmpty()) return;
		List<fEntity> entityList = new ArrayList<fEntity>();
		
		getEntityList().stream().filter(entry -> entry.getClass().equals(classs)).forEach(entityList::add);
		getObjectID().getPacketList().stream().filter(entry -> entry.getClass().equals(classs)).forEach(entityList::add);

		this.select(entityList.toArray(fEntity[]::new));
	}
	
	public void selectAdd(fEntity ... fEntity) {
		if(Objects.isNull(fEntity)) return;
		if(fEntity.length == 0) return;
		Arrays.asList(fEntity).forEach(entity -> {
			this.packetList.add(entity);
		});
		this.highLightEntities(this.packetList);
	}
	
	public void select(fEntity ... fEntity) {
		if(Objects.isNull(fEntity)) return;
		if(fEntity.length == 0) return;
		this.packetList.clear();
		this.selectAdd(fEntity);
	}
	
	public void deSelect(fEntity fEntity) {
		if(this.packetList.size() > 1) {
			this.packetList.remove(fEntity);
			this.select(this.packetList);
		}
	}
	
	public void removeEntities(List<fEntity> entityList) {
		entityList.stream().forEach(this::removeEntity);
		this.packetList.removeAll(entityList);
	}
	
	public void removeEntities(fEntity ... entityList) {
		Arrays.asList(entityList).forEach(this::removeEntity);
	}
	
	public void removeEntity(fEntity entity) {
		entity.kill();
		getObjectID().getPacketList().remove(entity);
	}
	
	public HashSet<fEntity> getEntityList(){
		return this.packetList;
	}
	
	public File save(UUID creator, PlaceableSide side) {
		try {
			File filePath = new File(FurnitureLib.isNewVersion() ? "plugins/FurnitureLib/models/" + this.modelName + ".dModel" : "plugins/FurnitureLib/Crafting/" + this.modelName + ".yml");
			if(!filePath.getParentFile().exists()) filePath.getParentFile().mkdirs();
			if(!filePath.exists()) filePath.createNewFile();
			
			FileConfiguration file = YamlConfiguration.loadConfiguration(filePath);
			
			file.options().header("------------------------------------  #\n"
					+ "                                      #\n"
					+ "      never touch the system-ID !     #\n"
					+ "                                      #\n"
					+ "------------------------------------  #\n");
			file.options().copyHeader(true);

			file.set(this.modelName + ".system-ID", this.modelName);
			file.set(this.modelName + ".creator", Objects.nonNull(creator) ? creator.toString() : null);
			
			if(FurnitureLib.isNewVersion()) {
				file.set(this.modelName + ".displayName", "&c" + this.modelName);
				file.set(this.modelName + ".spawnMaterial", FurnitureLib.getInstance().getDefaultSpawnMaterial().name());
				file.set(this.modelName + ".itemGlowEffect", false);
				file.set(this.modelName + ".itemLore", "");
			}else {
				file.set(this.modelName + ".name", "&c" + this.modelName);
				file.set(this.modelName + ".material", FurnitureLib.getInstance().getDefaultSpawnMaterial().name());
				file.set(this.modelName + ".glow", false);
				file.set(this.modelName + ".lore", "");
			}
			
			file.set(this.modelName + ".placeAbleSide", side.toString());
			file.set(this.modelName + ".crafting.disable", true);
			file.set(this.modelName + ".crafting.recipe", "xxx,xxx,xxx");
			file.set(this.modelName + ".crafting.index.x", Material.BEDROCK.name());
			
			AtomicInteger counter = new AtomicInteger(0);
			String modelData = FurnitureLib.isNewVersion() ? this.modelName + ".projectData.entities" : this.modelName + ".ProjectModels.ArmorStands";
			String blockData = FurnitureLib.isNewVersion() ? this.modelName + ".projectData.blockList" : this.modelName + ".ProjectModels.Block";
			ProjectMetadata projectMetadata = new ProjectMetadata();
			
			file.set(modelData, null);
			this.getObjectID().getPacketList().stream().forEach(packet -> {
				if(packet instanceof fInteraction) {
					fInteraction.class.cast(packet).setResponse(true);
				}
				
				if(packet instanceof fItem_display) {
					if(packet.getCustomName().startsWith("#SITZ")) {
						((fItem_display) packet).setItemStack(new ItemStack(Material.AIR));
						packet.setNameVasibility(false);
					}
				}
				
				file.set(modelData + "." + counter.getAndIncrement(), projectMetadata.toString(packet, getStartLocation()));
			});
			
			counter.set(0);
			if(!blockList.isEmpty()) {
				file.set(blockData, null);
				blockList.stream().forEach(block -> {
					String key = blockData + "." + counter.getAndIncrement() + ".";
					Relative relative = new Relative(block.getLocation(), getStartLocation().getBlock().getLocation());
					if(FurnitureLib.isNewVersion()) {
						file.set(key + "blockData", block.getBlockData().getAsString());
						file.set(key + "xOffset", relative.getOffsetX());
						file.set(key + "yOffset", relative.getOffsetY());
						file.set(key + "zOffset", relative.getOffsetZ());
						
						if(Skull.class.isInstance(block.getState())) {
							try {
								WrappedGameProfile profile = CraftSkull.extractGameProfile(block.getState());
								if(Objects.nonNull(profile)) {
									file.set(key + "gameProfile.uuid", profile.getUUID().toString());
									file.set(key + "gameProfile.name", profile.getName());
									profile.getProperties().asMap().entrySet().stream().forEach(entry -> {
										String propertieKey = key + "gameProfile." + entry.getKey();
										Collection<WrappedSignedProperty> collection = entry.getValue();
										if(!collection.isEmpty()) {
											collection.stream().forEach(propertie -> {
												file.set(propertieKey + ".value", propertie.getValue());
												file.set(propertieKey + ".signature", propertie.getSignature());
											});
										}
									});
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
						}
					}else {
						file.set(key + "Type", block.getType().name());
						file.set(key + "Data", block.getState().getData().getData());
						file.set(key + "X-Offset", relative.getOffsetX());
						file.set(key + "Y-Offset", relative.getOffsetY());
						file.set(key + "Z-Offset", relative.getOffsetZ());
						MaterialData data = block.getState().getData();
						if(data instanceof Directional){
							file.set(key + "Rotation", ((Directional) data).getFacing().name());
						}
						
						if(Skull.class.isInstance(block.getState())) {
							try {
								WrappedGameProfile profile = CraftSkull.extractGameProfile(block.getState());
								if(Objects.nonNull(profile)) {
									file.set(key + "gameProfile.uuid", profile.getUUID().toString());
									file.set(key + "gameProfile.name", profile.getName());
									profile.getProperties().asMap().entrySet().stream().forEach(entry -> {
										String propertieKey = key + "gameProfile." + entry.getKey();
										Collection<WrappedSignedProperty> collection = entry.getValue();
										if(!collection.isEmpty()) {
											collection.stream().forEach(propertie -> {
												file.set(propertieKey + ".value", propertie.getValue());
												file.set(propertieKey + ".signature", propertie.getSignature());
											});
										}
									});
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				});
			}else {
				file.set(blockData, null);
			}
			
			file.save(filePath);
			this.getObjectID().remove(false);
			return filePath;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public PlaceableSide getPlaceableSide() {
		return this.placeAbleSide;
	}
	
	private Project getProject(){
		for(Project project : FurnitureLib.getInstance().getFurnitureManager().getProjects()){
			if(project.getName().toLowerCase().equalsIgnoreCase(getModelName())){
				return project;
			}
		}
		return null;
	}
	
	public void stop() {
		this.area.reset(this.player);
		this.player = null;
		this.objectID.remove(player, false, false);
		this.objectID = null;
		this.area = null;
		this.modelName = null;
		this.blockList.clear();
		this.packetList.clear();
		this.placeAbleSide = null;
	}
	
	public Class<? extends fEntity> entitySelectorClass = null;
	
	public void setEntityType(Class<? extends fEntity> entitySelector) {
		this.entitySelectorClass = entitySelector;
	}
	
	public Class<? extends fEntity> getSelectedEntityType(){
		return this.entitySelectorClass;
	}
	
	public boolean isSelectedClass(fEntity entity) {
		if(Objects.nonNull(entity)) {
			if(Objects.isNull(entitySelectorClass)) return false;
			return fEntity.class.equals(entitySelectorClass);
		}
		return false;
	}
}