package de.Ste3et_C0st.DiceFunitureMaker;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.DiceFurnitureMaker.Commands.create;
import de.Ste3et_C0st.DiceFurnitureMaker.Commands.edit;
import de.Ste3et_C0st.DiceFurnitureMaker.Commands.importer;
import de.Ste3et_C0st.DiceFurnitureMaker.Commands.update;
import de.Ste3et_C0st.DiceFurnitureMaker.Commands.upload;
import de.Ste3et_C0st.FurnitureLib.Command.command;
import de.Ste3et_C0st.FurnitureLib.ModelLoader.ModelFileLoader;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureConfig;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.Type.PlaceableSide;

public class FurnitureMakerPlugin extends JavaPlugin implements CommandExecutor{
	
	private FurnitureLib lib;
	private static FurnitureMakerPlugin instance;
	private static FurnitureMakerManager furnitureMakerManager;
	public static FurnitureMakerPlugin getInstance(){return instance;}
	
	public void onEnable(){
		if(getServer().getPluginManager().isPluginEnabled("FurnitureLib") == false){
			this.disablePlugin("[FurnitureMaker] FurnitureLib is missing please install it!");
			System.out.println("You can find the download here: https://www.spigotmc.org/resources/furniturelibary-protectionlib.9368/");
			return;
		}
		
		if(FurnitureLib.getInstance().isEnabledPlugin() == true) {
			lib = (FurnitureLib) Bukkit.getPluginManager().getPlugin("FurnitureLib");
			instance = this;
			furnitureMakerManager = new FurnitureMakerManager();
			if(lib.getDescription().getVersion().startsWith("3")){
				FurnitureConfig.getFurnitureConfig().getLangManager().addText(YamlConfiguration.loadConfiguration(loadStream("EN_en.yml")));
				command.addCommand(new create("create", "create"));
				command.addCommand(new edit("edit", "edit"));
				command.addCommand(new upload("upload", "upload"));
				command.addCommand(new update("update", "update"));
				command.addCommand(new importer("import"));
			}else{
				lib.send("FurnitureLib Version > 2.x not found");
				lib.send("FurnitureMaker deos not load");
			}
		}else {
			this.disablePlugin("[FurnitureMaker] Plugin disabled because FurnitureLib is incorectly installed!");
		}
	}
	
	private void disablePlugin(String string) {
		System.out.println(string);
		Bukkit.getPluginManager().disablePlugin(this);
	}
	
	
	public void onDisable(){
		if(Objects.nonNull(furnitureMakerManager)) {
			furnitureMakerManager.furnitureMakerSet.stream().forEach(FurnitureMaker::stop);
		}
	}
	
	public FurnitureLib getFurnitureLib(){return lib;}
	
	public void registerProeject(String name, PlaceableSide side){
		File file = new File("plugins/FurnitureLib/models/", name+".dModel");
		ModelFileLoader.loadModelFile(file);
	}
	
	public BufferedReader loadStream(String str){
		if(!str.startsWith("/")) str = "/" + str;
		InputStream stream = getInstance().getClass().getResourceAsStream(str);
		try {
			return new BufferedReader(new InputStreamReader(stream, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public FurnitureMakerManager getManager() {
		return furnitureMakerManager;
	}
}