package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer;

import java.io.OutputStream;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.ModelLoader.Block.ModelBlock;
import de.Ste3et_C0st.FurnitureLib.Utilitis.InternalClassReader;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;

public abstract class ProjectTranslater{
	
	private String nbtString;
	private final FurnitureMaker maker;
	
	public ProjectTranslater(String nbtString, FurnitureMaker editor){
		this.nbtString = nbtString;
		this.maker = editor;
	}
	
	public abstract OutputStream reflectMojangParser(String str, OutputStream stream);
	public abstract fEntity parseEntity();
	public abstract ModelBlock parseBlock();
	
	public String getNbtString() {
		return this.nbtString;
	}
	
	public static String getPacketMainName() {
		String str = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
		return str.substring(0, str.lastIndexOf("_"));
	}
	
	public static Class<?> getNMSClass(String className) throws ClassNotFoundException{
		return Class.forName(InternalClassReader.NMS + className);
	}
	
	public static Class<?> getOBCClass(String className) throws ClassNotFoundException{
		return Class.forName(InternalClassReader.OBC + "." + className);
	}

	public FurnitureMaker getMaker() {
		return maker;
	}
	
	public ObjectID getObjectID() {
		return this.getMaker().getObjectID();
	}
	
	public Location getStartLocation() {
		return this.getMaker().getStartLocation();
	}
	
	public static double eval(String string)
    {
       double d = 0d;
       try{
           string = string.replace("~", "");
           if(string.isEmpty()) return d;
           if(string.contains("+")){
        	   string = string.replace("+", "");
        	   d+=Double.parseDouble(string);
        	   return d;
           }else if(string.contains("-")){
        	   string = string.replace("-", "");
        	   d-=Double.parseDouble(string);
        	   return d;
           }
           
           d+=Double.parseDouble(string);
       }catch(Exception ex){
    	   ex.printStackTrace();
       }
       
       return d;
    }
	
	
}