package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.v1_20;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.ProjectTranslater;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.TransformationParser;
import de.Ste3et_C0st.FurnitureLib.ModelLoader.Block.ModelBlock;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTCompressedStreamTools;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagCompound;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagList;
import de.Ste3et_C0st.FurnitureLib.Utilitis.MaterialConverter;
import de.Ste3et_C0st.FurnitureLib.Utilitis.Relative;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureManager;
import de.Ste3et_C0st.FurnitureLib.main.entity.fDisplay;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;

public class StringParser extends ProjectTranslater {

	private static Class<?> mojangParserClazz, NBTCompressedStreamToolsClazz, NBTTagCompoundClazz;
	private static boolean isRelocatedPaper = FurnitureLib.isVersionOrAbove("1.20.5") && FurnitureLib.isPaper();
	private List<fEntity> entities = new ArrayList<fEntity>();
	
	static {
		try {
			mojangParserClazz = isRelocatedPaper ? Class.forName("net.minecraft.nbt.TagParser") : Class.forName("net.minecraft.nbt.MojangsonParser");
			NBTCompressedStreamToolsClazz = isRelocatedPaper ? Class.forName("net.minecraft.nbt.NbtIo") : Class.forName("net.minecraft.nbt.NBTCompressedStreamTools"); //NbtIo
			NBTTagCompoundClazz = isRelocatedPaper ? Class.forName("net.minecraft.nbt.CompoundTag") : Class.forName("net.minecraft.nbt.NBTTagCompound"); //CompoundTag
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StringParser(String nbtString, FurnitureMaker maker) {
		super(nbtString.replaceAll("minecraft:", ""), maker);
		Location workingLocation = getMaker().getArea().getCenter().clone().add(0,0.5,0); //need to offset here to match with commandblock
		this.executeSummon(nbtString, workingLocation);
		getObjectID().addEntities(this.entities);
		getMaker().getEntityList().addAll(this.entities);
		
		this.entities.stream().forEach(entity -> {
			entity.send(getMaker().getPlayer());
			entity.update(getMaker().getPlayer());
		});
		
		getMaker().giveSide(0, null);
	}
	
	private void executeSummon(String command, Location startLocation){
		if(command.startsWith("/") == false) command = "/" + command;
		
		if(command.toLowerCase().startsWith("/summon ")) {
			String workingArguments = command.replaceFirst("/summon ", "");
			String[] args = workingArguments.split(" ", 5);
			
			if(args.length > 3) {
				final String minecraftMob = args[0].replace("minecraft:", "").toLowerCase();
				final String xString = args[1], yString = args[2], zString = args[3];
				final String nbtString = args.length > 4 ? args[4] : "{}";
				double xOffset = eval(xString);
				double yOffset = eval(yString);
				double zOffset = eval(zString);
				Location location = startLocation.clone().add(xOffset, yOffset, zOffset);
				
				Optional<EntityType> type = Stream.of(EntityType.values()).filter(entry -> entry.name().equalsIgnoreCase(minecraftMob)).findFirst();
				NBTTagCompound tagCompound = getNBTfromString(nbtString);
				if(type.isPresent() == true) {
					if(FurnitureManager.getInstance().isEntityTypeRegistred(type.get())) {
						placeEntity(type.get(), tagCompound, location).ifPresent(entity -> {
							this.entities.add(entity);
						});
					}else {
						this.parsePassenger(tagCompound, location);
					}
				}
			}
		}else if(command.toLowerCase().startsWith("/setblock")) {
			String workingArguments = command.replaceFirst("/setblock ", "");
			setBlock(workingArguments, startLocation);
		}
	}
	
	private void parsePassenger(final NBTTagCompound tagCompound, final Location location) {
		tagCompound.getCompound("Passengers", NBTTagList.class, tagList -> {
			tagList.stream().filter(NBTTagCompound.class::isInstance).map(NBTTagCompound.class::cast).forEach(passengerCompound -> {
				if(passengerCompound.hasKey("Command")) {
					executeSummon(passengerCompound.getString("Command"), location);
				}else {
					summon(passengerCompound, location).ifPresent(entity -> {});
				}
			});
		});
	}
	
	private Optional<fEntity> summon(final NBTTagCompound tagCompound,final Location startLocation) {
		String entityID = tagCompound.getString("id").toLowerCase().replaceFirst("minecraft:", "");
		Optional<EntityType> type = Stream.of(EntityType.values()).filter(entry -> entry.name().equalsIgnoreCase(entityID)).findFirst();
		return type.isPresent() ? this.placeEntity(type.get(), tagCompound, startLocation) : Optional.empty();
	}
	
	private Optional<fEntity> placeEntity(final EntityType entityType ,NBTTagCompound compound, Location startLocation) {
		Optional<fEntity> furnitureEntity = Optional.ofNullable(FurnitureLib.getInstance().getFurnitureManager().readEntity(entityType, startLocation, getObjectID()));
		
		furnitureEntity.ifPresent(entity -> {
			entity.loadMetadata(compound);
			if(entity instanceof fDisplay display) {
				compound.getCompound("transformation", NBTTagList.class, tagList -> {
					new TransformationParser().parse(tagList).ifPresent(transformation -> {
						display.setTransformation(transformation);
					});
				});
			}
			entities.add(entity);
		});
		
		this.parsePassenger(compound, startLocation);
		
		return furnitureEntity;
	}

	public fEntity parseEntity(List<NBTTagCompound> commandList) {

		return null;
	}

	public void setBlock(String str, Location location){
		str = str.replace("setblock ", "");
		String[] args = str.split(" ");
		String xOffsetStr = args[0];
		String yOffsetStr = args[1];
		String zOffsetStr = args[2];
		double xOffset = eval(xOffsetStr);
		double yOffset = eval(yOffsetStr);
		double zOffset = eval(zOffsetStr);
		final Relative relative = new Relative(location, xOffset, yOffset, zOffset, BlockFace.EAST);
		str = str.replace(xOffsetStr + " ", "");
		str = str.replace(yOffsetStr + " ", "");
		str = str.replace(zOffsetStr + " ", "");

		try {
			final Material m = MaterialConverter.getMaterialFromOld(args[3]);
			String byteStr = "0";

			try {
				if (args[4] != null) {
					String stl = args[4].replaceAll("[^\\d.]", "");
					byteStr = Integer.parseInt(stl) + "";
				}
			} catch (Exception ex) {
			}

			final int i = Integer.parseInt(byteStr);
			Bukkit.getScheduler().scheduleSyncDelayedTask(FurnitureLib.getInstance(), new Runnable() {
				@Override
				public void run() {
					relative.getSecondLocation().getBlock().setType(m);
					// relative.getSecondLocation().getBlock().setData((byte) i);
					getMaker().getBlockList().add(relative.getSecondLocation().getBlock());
				}
			});

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public NBTTagCompound getNBTfromString(String nbtString) {
		try (ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream(); OutputStream outputStream = reflectMojangParser(nbtString, byteOutputStream)) {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(byteOutputStream.toByteArray());
			return NBTCompressedStreamTools.read(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new NBTTagCompound();
	}

	@Override
	public OutputStream reflectMojangParser(String str, OutputStream stream) {
		try {
			Method parse = mojangParserClazz.getMethod(isRelocatedPaper ? "parseTag" : "a", String.class); //parseTag
			Method nbtToOutStream = NBTCompressedStreamToolsClazz.getMethod(isRelocatedPaper ? "writeCompressed" : "a", NBTTagCompoundClazz, OutputStream.class); //writeCompressed
			Object nbtTagCompound = parse.invoke(null, str.substring(str.indexOf("{"), str.length()));
			nbtToOutStream.invoke(null, nbtTagCompound, stream);
			return stream;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ModelBlock parseBlock() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public fEntity parseEntity() {
		// TODO Auto-generated method stub
		return null;
	}

}