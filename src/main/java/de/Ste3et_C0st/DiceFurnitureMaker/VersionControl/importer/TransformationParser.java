package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer;

import java.util.Optional;

import org.bukkit.util.Transformation;
import org.joml.Matrix4f;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagFloat;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagList;
import it.unimi.dsi.fastutil.floats.FloatArrayList;

public class TransformationParser {

//	private static Class<?> transformationClass = null;
//	private static Method translationMethod = null, leftRotationMethod = null, scaleMethod = null, rightRotationMethod = null;
//	
//	static {
//		if(FurnitureLib.getVersion(MinecraftVersion.fromServerVersion("1.19.4"))) {
//			try {
//				transformationClass = Class.forName("com.mojang.math.Transformation");
//				
//				translationMethod = transformationClass.getClass().getDeclaredMethod("d");
//				leftRotationMethod = transformationClass.getClass().getDeclaredMethod("e");
//				scaleMethod = transformationClass.getClass().getDeclaredMethod("f");
//				rightRotationMethod = transformationClass.getClass().getDeclaredMethod("g");
//			}catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
	
	public Optional<Transformation> parse(NBTTagList tagList) {
		try {
			FloatArrayList floatArrayList = new FloatArrayList(16);
			tagList.stream().map(NBTTagFloat.class::cast).map(NBTTagFloat::asFloat).limit(16).map(Float::floatValue).forEach(floatArrayList::add);
			Matrix4f matrix4f = new Matrix4f().set(floatArrayList.elements()).transpose();
			com.mojang.math.Transformation nmsTransformation = new com.mojang.math.Transformation(matrix4f);
			Transformation transformation = new Transformation(nmsTransformation.d(), nmsTransformation.e(), nmsTransformation.f(), nmsTransformation.g());
//			Vector3f fieldD = (Vector3f) translationMethod.invoke(nmsTransformation);
//			Quaternionf fieldE = (Quaternionf) leftRotationMethod.invoke(nmsTransformation);
//			Vector3f fieldF = (Vector3f) scaleMethod.invoke(nmsTransformation);
//			Quaternionf fieldG = (Quaternionf) rightRotationMethod.invoke(nmsTransformation);
			return Optional.of(transformation);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
	
}
