package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.v1_18;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;

import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.ProjectTranslater;
import de.Ste3et_C0st.FurnitureLib.ModelLoader.Block.ModelBlock;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTBase;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTCompressedStreamTools;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagCompound;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagList;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagString;
import de.Ste3et_C0st.FurnitureLib.Utilitis.MaterialConverter;
import de.Ste3et_C0st.FurnitureLib.Utilitis.Relative;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureManager;
import de.Ste3et_C0st.FurnitureLib.main.Type.BodyPart;
import de.Ste3et_C0st.FurnitureLib.main.entity.fArmorStand;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;

public class StringParser extends ProjectTranslater {

	private static Class<?> mojangParserClazz, NBTCompressedStreamToolsClazz, NBTTagCompoundClazz, NMSItem, CraftItemStack;

	static {
		try {
			mojangParserClazz = Class.forName("net.minecraft.nbt.MojangsonParser"); 
			NBTCompressedStreamToolsClazz = Class.forName("net.minecraft.nbt.NBTCompressedStreamTools");
			NBTTagCompoundClazz = Class.forName("net.minecraft.nbt.NBTTagCompound");
			NMSItem = Class.forName("net.minecraft.world.item.Item"); 
			CraftItemStack = ProjectTranslater.getOBCClass("inventory.CraftItemStack");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StringParser(String nbtString, FurnitureMaker maker) {
		super(nbtString.toLowerCase(), maker);
		List<NBTTagCompound> commandList = connvertCommands();
		List<fEntity> parsedEntities = parseEntities(commandList);
		parsedEntities.stream().forEach(entry -> {
			getObjectID().addArmorStand(entry);
			getMaker().addEntity(entry);
			entry.send(getMaker().getPlayer());
			entry.update(getMaker().getPlayer());
		});
		getMaker().giveSide(1, fArmorStand.class);
	}

	public List<fEntity> parseEntities(List<NBTTagCompound> commandList) {
		List<fEntity> entities = new ArrayList<fEntity>();
		Location startLocation = getStartLocation().clone().add(0, 3, 0);
		commandList.stream().forEach(compound -> {
			if (compound.hasKey("command")) {
				NBTTagString string = (NBTTagString) compound.get("command");
				String command = string.toString().toLowerCase();
				if (command.contains("summon armorstand") || command.contains("summon armor_stand")) {
					Optional<fEntity> entity = summonArmorStands(command, startLocation);
					if(entity.isPresent()) {
						entities.add(entity.get());
					}
				}else if(command.startsWith("setblock")) {
					Optional<Block> block = addBlock(command, startLocation);
					if(block.isPresent()) {
						getMaker().getBlockList().add(block.get());
					}
				}
			}
		});

		return entities;
	}
	
	public Optional<Block> addBlock(String command, Location startLocation){
		Optional<Block> optinal = Optional.empty();
		command = command.replace("setblock ", "");
		String[] args = command.split(" ");
		String xOffsetStr = args[0];
		String yOffsetStr = args[1];
		String zOffsetStr = args[2];
		double xOffset = eval(xOffsetStr);
		double yOffset = eval(yOffsetStr);
		double zOffset = eval(zOffsetStr);
		final Relative relative = new Relative(startLocation, xOffset, yOffset, zOffset, BlockFace.EAST);
		command = command.replace(xOffsetStr + " ", "").replace(yOffsetStr + " ", "").replace(zOffsetStr + " ", "");
		
		try{
			if(args[3].isEmpty()) return null;
			if(FurnitureLib.getVersionInt() > 12) {
				Material material = Material.AIR;
				material = MaterialConverter.getMaterialFromOld(args[3]);
				relative.getSecondLocation().getBlock().setType(material);
				optinal = Optional.of(relative.getSecondLocation().getBlock());
			}else {
				final Material m = Material.matchMaterial(args[3]);
				String byteStr = "0";
				try{
				if(args[4] != null){
						String stl = args[4].replaceAll("[^\\d.]", "");
						byteStr = Integer.parseInt(stl) + "";
				}}catch(Exception ex){}
				
				final int i = Integer.parseInt(byteStr);
				relative.getSecondLocation().getBlock().setType(m);
				relative.getSecondLocation().getBlock().getState().setRawData((byte) i);
				optinal = Optional.of(relative.getSecondLocation().getBlock());
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
		return optinal;
	}
	
	public Optional<fEntity> summonArmorStands(String command, Location startLocation) {
		command = command.replace("summon armorstand ", "summon armor_stand");
		
		int startIndexCommand = command.indexOf("summon armor_stand");
		command = command.substring(startIndexCommand, command.length());
		
		String[] args = command.replace("summon armor_stand ", "").split(" ");
		String xOffsetStr = args[0], yOffsetStr = args[1], zOffsetStr = args[2];
		double xOffset = eval(xOffsetStr), yOffset = eval(yOffsetStr), zOffset = eval(zOffsetStr);
		
		int index = command.contains("{") ? command.indexOf("{") : 0;
		
		int lastIndex = command.length();
		
		if(command.contains("{")) {
			int stack = 0;
			for (int i=index; i<command.length(); i++) {
				char c = command.charAt(i);
				if(c == '{') {
					stack++;
				}else if(c == '}') {
					stack--;
					if(stack == 0) {
						lastIndex = i + 1;
						break;
					}
				}
			}
		}
		
		String nbtString = command.substring(index, lastIndex);
		try (ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream(); OutputStream outputStream = reflectMojangParser(nbtString, byteOutputStream)) {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(byteOutputStream.toByteArray());
			NBTTagCompound metadata = NBTCompressedStreamTools.read(inputStream);
			final Relative relative = new Relative(startLocation, xOffset, yOffset, zOffset,BlockFace.EAST);
			Location armorStandLocation = relative.getSecondLocation();
			float yaw = startLocation.getYaw() + 180;
			
			if (metadata.hasKey("rotation")) {yaw +=metadata.getFloat("rotation");}
			
			armorStandLocation.setYaw(yaw);
			fArmorStand armorStand = FurnitureManager.getInstance().createArmorStand(getObjectID(),armorStandLocation);
			
			if (metadata.hasKey("customnamevisible")) {
				armorStand.setNameVisibility(metadata.getInt("customnamevisible") == 1);
			}
			if (metadata.hasKey("showarms")) {
				armorStand.setArms(metadata.getInt("showarms") == 1);
			}
			if (metadata.hasKey("small")) {
				armorStand.setSmall(metadata.getInt("small") == 1);
			}
			if (metadata.hasKey("marker")) {
				armorStand.setMarker(metadata.getInt("marker") == 0);
			}
			if (metadata.hasKey("glowing")) {
				armorStand.setGlowing(metadata.getInt("glowing") == 1);
			}
			if (metadata.hasKey("invisible")) {
				armorStand.setInvisible(metadata.getInt("invisible") == 1);
			}
			if (metadata.hasKey("nobaseplate")) {
				armorStand.setBasePlate(metadata.getInt("nobaseplate") == 0);
			}
			if (metadata.hasKey("customName")) {
				armorStand.setCustomName(
						ChatColor.translateAlternateColorCodes('&', metadata.getString("customName")));
			}
			if (metadata.hasKey("pose")) {
				NBTTagCompound pose = metadata.getCompound("pose");
				for (BodyPart part : BodyPart.values()) {
					EulerAngle angle = part.getDefAngle();
					String name = part.getName();
					name = name.replace("_", "");
					boolean b = false;
					if (pose.hasKey(name.toLowerCase())) {
						NBTTagList nbtPart = pose.getList(name.toLowerCase());
						if (nbtPart.size() == 3) {
							for (int i = 0; i < nbtPart.size(); i++) {
								String floatString = nbtPart.getString(i);
								Float f = Float.valueOf(floatString);
								double d = f;
								switch (i) {
								case 0:
									angle = angle.setX(d);
									break;
								case 1:
									angle = angle.setY(d);
									break;
								case 2:
									angle = angle.setZ(d);
									break;
								}
							}
						}
						b = true;
					}

					if (b) angle = FurnitureLib.getInstance().getLocationUtil().degresstoRad(angle);
					armorStand.setPose(angle, part);
				}
			}
			if (metadata.hasKey("handItems")) {
				for (int i = 0; i < metadata.getList("handItems").size(); i++) {
					NBTTagCompound item = metadata.getList("handitems").get(i);
						switch (i) {
							case 0:armorStand.setItemInMainHand(getStack(item));break;
							case 1:armorStand.setItemInOffHand(getStack(item));break;
						}
				}
			}
			if (metadata.hasKey("armoritems")) {
				for (int i = 0; i < metadata.getList("armoritems").size(); i++) {
					NBTTagCompound item = metadata.getList("armoritems").get(i);
						switch (i) {
							case 0:armorStand.setBoots(getStack(item));break;
							case 1:armorStand.setLeggings(getStack(item));break;
							case 2:armorStand.setChestPlate(getStack(item));break;
							case 3:armorStand.setHelmet(getStack(item));break;
						}
				}
			}

			return Optional.of(armorStand);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
	
	private ItemStack getStack(NBTTagCompound item){
		String materialName = item.getString("id");
		if(materialName.isEmpty()) return null;
		if(FurnitureLib.getVersionInt() > 12) {
			Material material = Material.AIR;
			if(item.hasKey("damage")) {
				item = MaterialConverter.convertNMSItemStack(item);
				if(!item.hasKey("id")) {
					item.setString("id", materialName);
				}else if(item.getString("id").isEmpty()) {
					item.setString("id", materialName);
				}
			}
			material = MaterialConverter.getMaterialFromOld(item.hasKey("damage") ? item.getString("id") : item.getString("id") + ":" + item.getInt("damage"));
			ItemStack stack = new ItemStack(material);
			return stack;
		}else {
			ItemStack stack = (ItemStack) getCraftItemStack(item.getString("id"));
			int a = 1;if(item.hasKey("count")) a = item.getInt("count");
			short d = 0;
			if(item.hasKey("damage")){
				d = (short) item.getInt("damage");
			}
			stack.setAmount(a);
			stack.setDurability(d);
			return stack;
		}
	}
	
	private Object getCraftItemStack(String id){
		try{
			Method m = NMSItem.getDeclaredMethod("b", String.class);
			Object item = m.invoke(null, id);
			
			m = CraftItemStack.getDeclaredMethod("asNewCraftStack", NMSItem);
			return m.invoke(null, item);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public fEntity parseEntity(List<NBTTagCompound> commandList) {

		return null;
	}

	public void setBlock(String str) throws Exception {
		str = str.replace("setblock ", "");
		String[] args = str.split(" ");
		String xOffsetStr = args[0];
		String yOffsetStr = args[1];
		String zOffsetStr = args[2];
		double xOffset = eval(xOffsetStr);
		double yOffset = eval(yOffsetStr);
		double zOffset = eval(zOffsetStr);
		final Relative relative = new Relative(getStartLocation(), xOffset, yOffset, zOffset, BlockFace.EAST);
		str = str.replace(xOffsetStr + " ", "");
		str = str.replace(yOffsetStr + " ", "");
		str = str.replace(zOffsetStr + " ", "");

		try {
			final Material m = MaterialConverter.getMaterialFromOld(args[3]);
			String byteStr = "0";

			try {
				if (args[4] != null) {
					String stl = args[4].replaceAll("[^\\d.]", "");
					byteStr = Integer.parseInt(stl) + "";
				}
			} catch (Exception ex) {
			}

			final int i = Integer.parseInt(byteStr);
			Bukkit.getScheduler().scheduleSyncDelayedTask(FurnitureLib.getInstance(), new Runnable() {
				@Override
				public void run() {
					relative.getSecondLocation().getBlock().setType(m);
					// relative.getSecondLocation().getBlock().setData((byte) i);
					getMaker().getBlockList().add(relative.getSecondLocation().getBlock());
				}
			});

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	List<NBTTagCompound> compounds = new ArrayList<NBTTagCompound>();

	public List<NBTTagCompound> connvertCommands() {
		try (ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
				OutputStream outputStream = reflectMojangParser(getNbtString(), byteOutputStream)) {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(byteOutputStream.toByteArray());
			if (inputStream.available() == 0)
				return null;
			NBTTagCompound compound = NBTCompressedStreamTools.read(inputStream);
			getCommandsRecrusive(compound, compounds);
			return compounds;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void getCommandsRecrusive(NBTTagCompound compound, List<NBTTagCompound> compoundList) {
		for (Object object : compound.c()) {
			if (object instanceof String) {
				String name = (String) object;
				NBTBase base = compound.get(name);
				if (base instanceof NBTTagList) {
					NBTTagList list = (NBTTagList) base;
					for (int i = 0; i < list.size(); i++) {
						NBTTagCompound compound2 = list.get(i);
						if (compound2.hasKey("command")) {
							compoundList.add(compound2);
						} else {
							getCommandsRecrusive(compound2, compoundList);
						}
					}
				}
			}
		}
	}

	@Override
	public OutputStream reflectMojangParser(String str, OutputStream stream) {
		try {
			Method parse = mojangParserClazz.getMethod("a", String.class);
			Method nbtToOutStream = NBTCompressedStreamToolsClazz.getMethod("a", NBTTagCompoundClazz,
					OutputStream.class);

			Object nbtTagCompound = parse.invoke(null, str.substring(str.indexOf("{"), str.length()));
			nbtToOutStream.invoke(null, nbtTagCompound, stream);
			return stream;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ModelBlock parseBlock() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public fEntity parseEntity() {
		// TODO Auto-generated method stub
		return null;
	}

}