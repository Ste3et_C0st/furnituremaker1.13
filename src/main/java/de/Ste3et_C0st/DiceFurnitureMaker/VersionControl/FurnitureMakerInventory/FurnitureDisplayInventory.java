package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.FurnitureMakerInventory;

import java.awt.Color;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Particle.DustOptions;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import com.google.common.base.Supplier;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.BlockDisplayInventory;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.DisplaySettings;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.EntitySelectorInventory;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.ItemDisplayInventory;
import de.Ste3et_C0st.DiceFunitureMaker.Flags.SpawnEntityInventory;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMakerInventory;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ChatInputHandler;
import de.Ste3et_C0st.FurnitureLib.Utilitis.DoubleKey;
import de.Ste3et_C0st.FurnitureLib.Utilitis.ItemStackBuilder;
import de.Ste3et_C0st.FurnitureLib.main.entity.SizeableEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fBlock_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fDisplay;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fInteraction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fItem_display;
import de.Ste3et_C0st.FurnitureLib.main.entity.fSize;
import de.Ste3et_C0st.FurnitureLib.main.entity.fText_display;
import de.Ste3et_C0st.FurnitureLib.main.interfaces.ScaleableEntity;

public class FurnitureDisplayInventory{

	private final FurnitureMakerInventory inventory;
	private final Player player;
	
	private float[] aList = {2f,1.75f,1.5f,1.25f,1f,.75f,.5f,.25f,.125f, .0625f, .03125f};
	private int upscaleInt = 4, upscaleSize = 4;
	private int upscaleSelector = 0, upscaleMode = 0;
	private int mode = 0;
	private final Supplier<String> transformationName = () -> "§3Transformation [§c" + this.getMode() +  "§3," + getUpscaleState() + "§3]: §e" + this.getUpscaleFloat();
	private final Supplier<String> sizeName = () -> "§3Change Size [" + this.getModeSize() + "§3]: §e" + this.getUpscaleStateSize();
	private final Supplier<String> slimeSize = () -> "§3Change Size: §e" + this.getUpscaleStateSize();
	
	public FurnitureDisplayInventory(Player player, FurnitureMakerInventory inventory) {
		this.inventory = inventory;
		this.player = player;
		
		inventory.stackList.put(new DoubleKey<Integer>(0, 0), ItemStackBuilder.of(Material.PIG_SPAWN_EGG).setName("§9Add Entity").build());
		
		//blockDisplay
		inventory.stackList.put(new DoubleKey<Integer>(3, 0), ItemStackBuilder.of(Material.STONE).setName("§9Spawn BlockDisplay").build());
		inventory.stackList.put(new DoubleKey<Integer>(3, 1), inventory.getItem(1, 1).get());
		inventory.stackList.put(new DoubleKey<Integer>(3, 2), new ItemStackBuilder(Material.WHITE_SHULKER_BOX).setName("Change Block Item").build());
		inventory.stackList.put(new DoubleKey<Integer>(3, 3), new ItemStackBuilder(Material.COMPASS).setName(transformationName.get())
				.setLore(
						"§7Sneek + Scroll: §3Transformation (§aScale|§2Rotate)",
						"§8§m----------------------------------",
						"§7Drop Item: Switch Mode (§aScale§7|§2Rotate)",
						"§7RightClick: change working area (§dXYZ§7|§dX§7|§dY§7|§dZ§7)",
						"§7LeftClick: change working area (§dXYZ§7|§dX§7|§dY§7|§dZ§7)",
						"§7Sneek + RightClick: §cdecrease §eworking size",
						"§7Sneek + LeftClick: §2increase §eworking size"
			    ).build());
		inventory.stackList.put(new DoubleKey<Integer>(3, 4), new ItemStackBuilder(Material.ITEM_FRAME).setName("Edit Display").build());
		inventory.stackList.put(new DoubleKey<Integer>(3, 5), new ItemStackBuilder(Material.TARGET).setName(sizeName.get())
				.setLore(
						"§7Sneek + Scroll: §7Change Size",
						"§8§m----------------------------------",
						"§7RightClick: change mode (§dwidth§7|§dheight§7)",
						"§7LeftClick: change mode (§dwidth§7|§dheight§7)",
						"§7Sneek + RightClick: §cdecrease §eworking size",
						"§7Sneek + LeftClick: §2increase §eworking size"
				).build());
		inventory.stackList.put(new DoubleKey<Integer>(3, 7), inventory.getItem(1, 7).get());
		
		//textDisplay
		inventory.stackList.put(new DoubleKey<Integer>(4, 0), ItemStackBuilder.of(Material.NAME_TAG).setName("§9Spawn TextDisplay").build());
		inventory.stackList.put(new DoubleKey<Integer>(4, 1), inventory.getItem(1, 1).get());
		inventory.stackList.put(new DoubleKey<Integer>(4, 2), new ItemStackBuilder(Material.ENCHANTED_BOOK).setName("Change Text").build());
		inventory.stackList.put(new DoubleKey<Integer>(4, 3), inventory.getItem(3, 3).get());
		inventory.stackList.put(new DoubleKey<Integer>(4, 4), inventory.getItem(3, 4).get());
		inventory.stackList.put(new DoubleKey<Integer>(4, 5), inventory.getItem(3, 5).get());
		inventory.stackList.put(new DoubleKey<Integer>(4, 7), inventory.getItem(1, 7).get());
		
		//itemDisplay
		inventory.stackList.put(new DoubleKey<Integer>(5, 0), ItemStackBuilder.of(Material.DIAMOND_SWORD).setName("§9Spawn ItemDisplay").build());
		inventory.stackList.put(new DoubleKey<Integer>(5, 1), inventory.getItem(1, 1).get());
		inventory.stackList.put(new DoubleKey<Integer>(5, 2), new ItemStackBuilder(Material.COAL).setName("Change Item").build());
		inventory.stackList.put(new DoubleKey<Integer>(5, 3), inventory.getItem(3, 3).get());
		inventory.stackList.put(new DoubleKey<Integer>(5, 5), inventory.getItem(3, 5).get());
		inventory.stackList.put(new DoubleKey<Integer>(5, 4), inventory.getItem(3, 4).get());
		inventory.stackList.put(new DoubleKey<Integer>(5, 7), inventory.getItem(1, 7).get());
		
		//interactionHitbox
		inventory.stackList.put(new DoubleKey<Integer>(6, 0), ItemStackBuilder.of(Material.OAK_BUTTON).setName("§9Spawn Interaction HitBox").build());
		inventory.stackList.put(new DoubleKey<Integer>(6, 1), inventory.getItem(1, 1).get());
		inventory.stackList.put(new DoubleKey<Integer>(6, 2), inventory.getItem(3, 5).get());
		inventory.stackList.put(new DoubleKey<Integer>(6, 7), inventory.getItem(1, 7).get());
		
		inventory.stackList.put(new DoubleKey<Integer>(3, 8), inventory.getItem(1, 8).get());
		inventory.stackList.put(new DoubleKey<Integer>(4, 8), inventory.getItem(1, 8).get());
		inventory.stackList.put(new DoubleKey<Integer>(5, 8), inventory.getItem(1, 8).get());
		inventory.stackList.put(new DoubleKey<Integer>(6, 8), inventory.getItem(1, 8).get());
		
		inventory.stackList.put(new DoubleKey<Integer>(7, 2), ItemStackBuilder.of(Material.FLINT).setName(sizeName.get())
				.setLore(
						"§7Sneek + Scroll: §7Change Size",
						"§8§m----------------------------------",
						"§7RightClick: change mode (§dwidth§7|§dheight§7)",
						"§7LeftClick: change mode (§dwidth§7|§dheight§7)"
				).build());
		
		inventory.stackList.put(new DoubleKey<Integer>(2, 0), ItemStackBuilder.of(Material.REDSTONE).setName("§6► Editor").build());
	}
	
	public void onTick() {
		final Class<? extends fEntity> clazz = inventory.getSelectedEntityType();
		if(Objects.nonNull(clazz)) {
			if(clazz == fInteraction.class) {
				this.showSize();
			}else if(fSize.class.isAssignableFrom(clazz)) {
				if(Material.TARGET == player.getInventory().getItemInMainHand().getType()) {
					this.showSize();
				}
			}
		}
	}
	
	private void showSize() {
		inventory.getEntityList().stream().filter(Objects::nonNull).map(fSize.class::cast).forEach(entry -> {
			final Location startLocation = entry.getLocation().clone().subtract(entry.getWidth() / 2, 0, entry.getWidth() / 2);
			final Location endLocation =  entry.getLocation().clone().add(entry.getWidth() / 2, entry.getHeight(), entry.getWidth() / 2);
			final World world = startLocation.getWorld();
			
			final List<Vector> locationList = showCuboid(startLocation, endLocation, .25);
			final Color color = getRandomColor(entry.getUUID());
			final DustOptions option = new DustOptions(org.bukkit.Color.fromRGB(color.getRed(), color.getGreen(), color.getBlue()), 1f);
			
			locationList.stream().forEach(loc -> {
				world.spawnParticle(Particle.REDSTONE, loc.toLocation(world), 1, option);
			});
		});
	}
	
    public static Color getRandomColor(UUID id) {

        byte[] bytes = UUID2Bytes(id);

        int r= Math.abs(bytes[0]);
        int g = Math.abs(bytes[1]);
        int b = Math.abs(bytes[2]);

        return  new Color(r, g, b).brighter();
    }
    
    public static byte[] UUID2Bytes(UUID uuid) {
        long hi = uuid.getMostSignificantBits();
        long lo = uuid.getLeastSignificantBits();
        return ByteBuffer.allocate(16).putLong(hi).putLong(lo).array();
    }
	
    public List<Location> getHollowCube(Location corner1, Location corner2, double particleDistance) {
        List<Location> result = new ArrayList<Location>();
        World world = corner1.getWorld();
        double minX = Math.min(corner1.getX(), corner2.getX());
        double minY = Math.min(corner1.getY(), corner2.getY());
        double minZ = Math.min(corner1.getZ(), corner2.getZ());
        double maxX = Math.max(corner1.getX(), corner2.getX());
        double maxY = Math.max(corner1.getY(), corner2.getY());
        double maxZ = Math.max(corner1.getZ(), corner2.getZ());
     
        for (double x = minX; x <= maxX; x+=particleDistance) {
            for (double y = minY; y <= maxY; y+=particleDistance) {
                for (double z = minZ; z <= maxZ; z+=particleDistance) {
                    int components = 0;
                    if (x == minX || x == maxX) components++;
                    if (y == minY || y == maxY) components++;
                    if (z == minZ || z == maxZ) components++;
                    if (components >= 2) {
                        result.add(new Location(world, x, y, z));
                    }
                }
            }
        }
     
        return result;
    }
    
	
	public List<Vector> showCuboid(Location aLoc, Location bLoc, double step) {
	    List<Vector> result = new ArrayList<Vector>();
	    double[] xArr = {Math.min(aLoc.getX(), bLoc.getX()), Math.max(aLoc.getX(), bLoc.getX())};
	    double[] yArr = {Math.min(aLoc.getY(), bLoc.getY()), Math.max(aLoc.getY(), bLoc.getY())};
	    double[] zArr = {Math.min(aLoc.getZ(), bLoc.getZ()), Math.max(aLoc.getZ(), bLoc.getZ())};

	    for (double x = xArr[0]; x < xArr[1]; x += step) for (double y : yArr) for (double z : zArr) {
	    	result.add(new Vector(x, y, z));
	    }
	    for (double y = yArr[0]; y < yArr[1]; y += step) for (double x : xArr) for (double z : zArr) {
	    	result.add(new Vector(x, y, z));
	    }
	    for (double z = zArr[0]; z < zArr[1]; z += step) for (double y : yArr) for (double x : xArr) {
	    	result.add(new Vector(x, y, z));
	    }
	    return result;
	}

	public boolean onItemSlotForward(ItemStack stack) {
		if(stack.getType() == Material.COMPASS) {
			if(mode == 0) this.upsale(true);
			if(mode == 1) this.rotate(true);
			return true;
		}else if(stack.getType() == Material.TARGET || stack.getType() == Material.FLINT) {
			this.changeSize(true);
			return true;
		}
		return false;
	}

	public boolean onItemSlotBackward(ItemStack stack) {
		if(stack.getType() == Material.COMPASS) {
			if(mode == 0) this.upsale(false);
			if(mode == 1) this.rotate(false);
			return true;
		}else if(stack.getType() == Material.TARGET || stack.getType() == Material.FLINT) {
			this.changeSize(false);
			return true;
		}
		return false;
	}
	
	private void rotate(boolean add) {
		if(this.inventory.getEntitySize(this.inventory.getSelectedEntityType()) > 0) {
			final float upscaleFloat = getUpscaleFloat();
			final float upscaleSelector = this.upscaleSelector;
			final float upscaleFloatX = upscaleSelector == 0 || upscaleSelector == 1 ? 1 : 0;
			final float upscaleFloatY = upscaleSelector == 0 || upscaleSelector == 2 ? 1 : 0;
			final float upscaleFloatZ = upscaleSelector == 0 || upscaleSelector == 3 ? 1 : 0;
			
			final float angleFloat = (float) (upscaleFloat / 180 * Math.PI);
			
			this.inventory.getEntityList().stream().map(fDisplay.class::cast).forEach(entry -> {
				if(add) {
					final Matrix4f d = new Matrix4f();
					
					final Quaternionf axis = new Quaternionf(entry.getRightRotationObj());
					
					d.rotate(axis);
					d.rotate(angleFloat, new Vector3f(upscaleFloatX,upscaleFloatY,upscaleFloatZ));
					
					final Quaternionf newRotation = axis.setFromUnnormalized(d);
					entry.setRightRotation(newRotation);
				}else {
					final Matrix4f d = new Matrix4f();
					final Quaternionf axis = new Quaternionf(entry.getRightRotationObj());
					
					d.rotate(axis);
					d.rotate(-angleFloat, new Vector3f(upscaleFloatX,upscaleFloatY,upscaleFloatZ));
					final Quaternionf newRotation = axis.setFromUnnormalized(d);
					entry.setRightRotation(newRotation);
				}
				
				entry.update(this.player);
			});
		}
	}
	
	private void changeSize(boolean add) {
		if(this.inventory.getEntitySize(this.inventory.getSelectedEntityType()) > 0) {
			final float width = upscaleMode == 0 || upscaleMode == 1 ? getUpscaleStateSize() : 0;
			final float height = upscaleMode == 0 || upscaleMode == 2 ? getUpscaleStateSize() : 0;
			
			this.inventory.getEntityList().stream().filter(fSize.class::isInstance).map(fSize.class::cast).forEach(entry -> {
				if(add) {
					entry.setWidth(entry.getWidth() + width);
					entry.setHeight(entry.getHeight() + height);
				}else if(add == false) {
					entry.setWidth(entry.getWidth() - width);
					entry.setHeight(entry.getHeight() - height);
				}
				
				entry.update(player);
			});
			
			this.inventory.getEntityList().stream().filter(ScaleableEntity.class::isInstance).forEach(entry -> {
				final ScaleableEntity entity = ScaleableEntity.class.cast(entry);
				double baseValue = entity.getScale();
				final double change;
				if(add) {
					change = baseValue + width < 10D ? baseValue + width : 10D;
				}else {
					change = baseValue - width > 0.2D ? baseValue - width : 0.2D;
				}
				
				entity.setScale(Math.round(change * 100D) / 100D).update(player);
			});
		}
	}
	
	private void upsale(boolean add) {
		if(this.inventory.getEntitySize(this.inventory.getSelectedEntityType()) > 0) {
			final float upscaleFloat = getUpscaleFloat();
			final float upscaleSelector = this.upscaleSelector;
			final float upscaleFloatX = upscaleSelector == 0 || upscaleSelector == 1 ? upscaleFloat : 0;
			final float upscaleFloatY = upscaleSelector == 0 || upscaleSelector == 2 ? upscaleFloat : 0;
			final float upscaleFloatZ = upscaleSelector == 0 || upscaleSelector == 3 ? upscaleFloat : 0;
			
			this.inventory.getEntityList().stream().map(fDisplay.class::cast).forEach(entry -> {
				if(add) {
					entry.setScale(new Vector3f(entry.getScale()).add(upscaleFloatX, upscaleFloatY, upscaleFloatZ));
				}else {
					final Vector3f newVector = new Vector3f(entry.getScale()).sub(upscaleFloatX, upscaleFloatY, upscaleFloatZ);
					if(newVector.x < .03125f) newVector.x = .03125f;
					if(newVector.y < .03125f) newVector.y = .03125f;
					if(newVector.z < .03125f) newVector.z = .03125f;
					
					entry.setScale(newVector);
				}
				entry.update(this.player);
			});
		}
	}

	public boolean onItemStackRightClick(ItemStack stack) {
		if(inventory.isItem(stack)) {
			if(stack.getType() == Material.TARGET) {
				if(player.isSneaking()) {
					if(upscaleSize < this.aList.length) {
						upscaleSize++;
						this.inventory.rebuildStack(stack, sizeName);
					}
				}else {
					if(upscaleMode < 3) {
						upscaleMode++;
						this.inventory.rebuildStack(stack, sizeName);
					}
				}
			}else if(stack.getType() == Material.OAK_BUTTON) {
				FurnitureMakerPlugin.getInstance().getManager().getMaker(player).addEntity(fInteraction.class);
				return true;
			}else if(stack.getType() == Material.NAME_TAG) {
				FurnitureMakerPlugin.getInstance().getManager().getMaker(player).addEntity(fText_display.class);
				return true;
			}else if(stack.getType() == Material.DIAMOND_SWORD) {
				FurnitureMakerPlugin.getInstance().getManager().getMaker(player).addEntity(fItem_display.class);
				return true;
			}else if(stack.getType() == Material.REDSTONE) {
				new EntitySelectorInventory(player);
				return true;
			}else if(stack.getType() == Material.PIG_SPAWN_EGG) {
				new SpawnEntityInventory(player);
				return true;
			}else if(stack.getType() == Material.STONE) {
				FurnitureMakerPlugin.getInstance().getManager().getMaker(player).addEntity(fBlock_display.class);
				return true;
			}else if(stack.getType() == Material.WHITE_SHULKER_BOX) {
				if(this.inventory.getEntitySize(this.inventory.getSelectedEntityType()) > 0) {
					new BlockDisplayInventory(player);
				}else {
					player.sendMessage("§cPlease select/spawn an entity to open the BlockDisplay Inventory.");
				}
				return true;
			}else if(stack.getType() == Material.COMPASS) {
				if(player.isSneaking()) {
					if(upscaleInt < this.aList.length) {
						upscaleInt++;
						this.inventory.rebuildStack(stack, transformationName);
					}
				}else {
					if(upscaleSelector < 3) {
						upscaleSelector++;
						this.inventory.rebuildStack(stack, transformationName);
					}
				}
				return true;
			}else if(stack.getType() == Material.ITEM_FRAME) {
				if(this.inventory.getEntitySize(this.inventory.getSelectedEntityType()) > 0) {
					new DisplaySettings(player);
				}else {
					player.sendMessage("§cPlease select/spaw an entity to open the Display Settings.");
				}
				return true;
			}else if(stack.getType() == Material.COAL) {
				if(this.inventory.getEntityList().size() < 1) return true;
				new ItemDisplayInventory(player);
				return true;
			}else if(stack.getType() == Material.ENCHANTED_BOOK) {
				if(this.inventory.getEntitySize(this.inventory.getSelectedEntityType()) > 0) {
					new ChatInputHandler(player, string -> {
						return true;
					}, returnValue -> {
						if(returnValue.getState() == ChatInputHandler.ReturnState.SUCCESS) {
							player.sendMessage("Display Text have been Updatet");
							final String chatString = ChatColor.translateAlternateColorCodes('&', returnValue.getInput());
							this.inventory.getEntityList().stream().map(fText_display.class::cast).forEach(entry -> {
								entry.setText(chatString);
								entry.update(player);
							});
						}else {
							player.sendMessage("Please try again to insert a Display Text");
						}
					}, onOpen -> {
							player.sendMessage("Please insert a Display Text, do you have 20 sec for it");
					}, Duration.ofSeconds(20));
				}else {
					player.sendMessage("§7Please select or Spawn an Entity");
				}
				
				return true;
			}
		}
		
		return false;
	}

	public boolean onItemStackLeftClick(ItemStack stack) {
		if(stack.getType() == Material.COMPASS) {
			if(player.isSneaking()) {
				if(upscaleInt > 0) {
					upscaleInt--;
					this.inventory.rebuildStack(stack, transformationName);
				}
			}else {
				if(upscaleSelector > 0) {
					upscaleSelector--;
					this.inventory.rebuildStack(stack, transformationName);
				}
			}
			return true;
		}else if(stack.getType() == Material.TARGET) {
			if(player.isSneaking()) {
				if(upscaleSize > 0) {
					upscaleSize--;
					this.inventory.rebuildStack(stack, sizeName);
				}
			}else {
				if(upscaleMode > 0) {
					upscaleMode--;
					this.inventory.rebuildStack(stack, sizeName);
				}
			}
			return true;
		}
		return false;
	}
	
	public ItemStack buildScaleStack() {
		return new ItemStackBuilder(Material.COMPASS).setName(transformationName.get()).build();
	}
	
	private String getMode() {
		return this.mode == 0 ? "§aScale" : "§2Rotate";
	}
	
	private String getModeSize() {
		switch (upscaleMode) {
			case 1: return "§dwidth";
			case 2: return "§dheight";
			default: return "§dwidth/height";
		}
	}
	
	private String getUpscaleState() {
		switch (this.upscaleSelector) {
			case 1: return "§dX";
			case 2: return "§dY";
			case 3: return "§dZ";
			case 4: return mode == 1 ? "§dangle" : "XYZ";
			default: return "§dXYZ";
		}
	}
	
	public float getUpscaleFloat() {
		float upscale = 1f;
		if(this.upscaleInt < this.aList.length && this.upscaleInt > -1) {
			upscale = this.aList[this.upscaleInt];
		}
		return upscale;
	}
	
	public float getUpscaleStateSize() {
		float upscale = 1f;
		if(this.upscaleSize < this.aList.length && this.upscaleSize > -1) {
			upscale = this.aList[this.upscaleSize];
		}
		return upscale;
	}

	public boolean onItemDrop(ItemStack stack) {
		if(stack.getType() == Material.COMPASS) {
			this.mode = this.mode == 0 ? 1 : 0;
			this.inventory.rebuildStack(stack, transformationName);
			return true;
		}
		return false;
	}

	public boolean onItemPointer(ItemStack stack) {
		return false;
	}
}
