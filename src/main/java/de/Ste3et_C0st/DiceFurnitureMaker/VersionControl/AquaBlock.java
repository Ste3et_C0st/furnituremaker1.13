package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.wrappers.BlockPosition;
import com.comphenix.protocol.wrappers.WrappedBlockData;

public class AquaBlock extends PacketBlock{

	private Material material;
	
	public AquaBlock(Location location, Material material) {
		super(location);
		this.material = material;
		BlockPosition pos = new BlockPosition(getLocation().toVector());
		getHandle().getBlockPositionModifier().write(0, pos);
	}

	@Override
	public void parseBlock(Player player) {
		WrappedBlockData blockData = WrappedBlockData.createData(material);
		getHandle().getBlockData().write(0, blockData);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, getHandle());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void resetBlock(Player player) {
		Block block = getLocation().getBlock();
		WrappedBlockData blockData = WrappedBlockData.createData(block.getType());
		getHandle().getBlockData().write(0, blockData);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, getHandle());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
