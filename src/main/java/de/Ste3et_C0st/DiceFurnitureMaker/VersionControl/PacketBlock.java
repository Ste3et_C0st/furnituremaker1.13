package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;

public abstract class PacketBlock {

	private Location location;
	private PacketContainer containter = new PacketContainer(PacketType.Play.Server.BLOCK_CHANGE);
	
	public PacketBlock(Location location) {
		this.location = location;
	}
	
	public abstract void parseBlock(Player player);
	public abstract void resetBlock(Player player);
	
	public Location getLocation() {
		return this.location;
	}
	
	protected PacketContainer getHandle() {
		return this.containter;
	}
}
