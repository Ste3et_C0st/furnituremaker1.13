package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl;

import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.ProjectTranslater;
import de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.StringParser;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;

public class VersionHandler {

//	private static Class<?> translater;
//	
//	static {
//		try {
//			translater = Class.forName("de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer." + ProjectTranslater.getPacketMainName() + ".StringParser");
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//	}
//	
	public static ProjectTranslater furnitureMojangsonParser(String nbtString, final FurnitureMaker maker) {
//		if(Objects.nonNull(translater)) {
//			try {
//				return (ProjectTranslater) translater.getConstructor(String.class, FurnitureMaker.class).newInstance(nbtString, maker);
//			}catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
		
		if(FurnitureLib.getVersionInt() == 17) {
			return new de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.v1_17.StringParser(nbtString, maker);
		}else if(FurnitureLib.getVersionInt() == 18) {
			return new de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.v1_18.StringParser(nbtString, maker);
		}else if(FurnitureLib.getVersionInt() == 20 || FurnitureLib.getVersionInt() == 21) {
			return new de.Ste3et_C0st.DiceFurnitureMaker.VersionControl.importer.v1_20.StringParser(nbtString, maker);
		}
		
		return new StringParser(nbtString, maker);
	}
	
}
