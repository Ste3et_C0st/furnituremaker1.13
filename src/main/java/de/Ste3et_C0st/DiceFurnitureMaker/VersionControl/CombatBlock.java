package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.wrappers.BlockPosition;
import com.comphenix.protocol.wrappers.WrappedBlockData;

public class CombatBlock extends PacketBlock{

	private Material material;
	private short durability;
	
	public CombatBlock(Location location, Material material, short durability) {
		super(location);
		this.material = material;
		this.durability = durability;
		BlockPosition pos = new BlockPosition(getLocation().toVector());
		getHandle().getBlockPositionModifier().write(0, pos);
	}

	@Override
	public void parseBlock(Player player) {
		WrappedBlockData blockData = WrappedBlockData.createData(material, durability);
		getHandle().getBlockData().write(0, blockData);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, getHandle());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void resetBlock(Player player) {
		Block block = getLocation().getBlock();
		WrappedBlockData blockData = WrappedBlockData.createData(block.getType(), block.getData());
		getHandle().getBlockData().write(0, blockData);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, getHandle());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
