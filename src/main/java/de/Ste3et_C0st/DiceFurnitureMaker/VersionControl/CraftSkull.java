package de.Ste3et_C0st.DiceFurnitureMaker.VersionControl;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import org.bukkit.block.BlockState;
import org.bukkit.block.Skull;

import com.comphenix.protocol.wrappers.WrappedGameProfile;

import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;

public class CraftSkull {

	private static HashMap<String, WrappedGameProfile> gameProfileChache = new HashMap<String, WrappedGameProfile>();
	
	public static WrappedGameProfile extractGameProfile(BlockState state) {
		if(Objects.nonNull(state)) {
			if(Skull.class.isInstance(state)) {
				Skull skull = Skull.class.cast(state);
				if(skull.hasOwner()) {
					if(FurnitureLib.isNewVersion()) {
						if(gameProfileChache.containsKey(skull.getOwningPlayer().getUniqueId().toString())) {
							return gameProfileChache.get(skull.getOwningPlayer().getUniqueId().toString());
						}
					}
				}
				
				try {
					Class<?> craftSkullClazz = Class.forName("org.bukkit.craftbukkit." + FurnitureLib.getBukkitVersion() + ".block.CraftSkull");
					if(craftSkullClazz.isInstance(skull)) {
						Object craftSkull = craftSkullClazz.cast(skull);
						Field field = craftSkullClazz.getDeclaredField("profile");
						field.setAccessible(true);
						Object gameProfile = field.get(craftSkull);
						if(Objects.nonNull(gameProfile)) {
							WrappedGameProfile profile = WrappedGameProfile.fromHandle(gameProfile);
							gameProfileChache.put(profile.getUUID().toString(), profile);
							return profile;
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
}
