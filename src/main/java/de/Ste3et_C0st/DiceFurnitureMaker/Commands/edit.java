package de.Ste3et_C0st.DiceFurnitureMaker.Commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.DiceFunitureMaker.Maker.FurnitureMaker;
import de.Ste3et_C0st.FurnitureLib.Command.command;
import de.Ste3et_C0st.FurnitureLib.Command.iCommand;
import de.Ste3et_C0st.FurnitureLib.Crafting.Project;
import de.Ste3et_C0st.FurnitureLib.SchematicLoader.ProjectLoader;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.SQLAction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;
import de.Ste3et_C0st.FurnitureLib.main.entity.fInteraction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fItem_display;

public class edit extends iCommand{
	
	public edit(String subCommand, String ...args) {
		super(subCommand);
		setTab("installedDModels");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof Player){
			if(args.length!=2){command.sendHelp((Player) sender);return;}
			if(args[0].equalsIgnoreCase("edit")){
				String name = args[1];
				Project project = isExist(name);
				if(project==null||!project.isEditorProject()){
					sender.sendMessage("§cThe Model deosnt exist !");
					return;
				}else{
					if(!hasCommandPermission(sender)) return;
					Location loc = ((Player) sender).getLocation().getBlock().getLocation();
					loc.setYaw(FurnitureLib.getInstance().getLocationUtil().FaceToYaw(BlockFace.NORTH.getOppositeFace()));
					ObjectID id = new ObjectID(project.getName(), project.getPlugin().getName(), loc);
					id.setPrivate(true);
					if(project.isEditorProject()){new ProjectLoader(id, false).getProject().getModelschematic().spawn(id);}else{FurnitureLib.getInstance().spawn(project,id);}
					FurnitureMaker maker = FurnitureMakerPlugin.getInstance().getManager().startMaker((Player) sender, loc, name, id);
					
					id.getPacketList().stream().filter(Objects::nonNull).forEach(entity -> {
						if(entity instanceof fInteraction) {
							fInteraction.class.cast(entity).setResponse(false);
						}
						
						if(entity.getCustomName().startsWith("#SITZ") && entity instanceof fItem_display) {
							fItem_display.class.cast(entity).setItemStack(new ItemStack(Material.REDSTONE_TORCH));
						}
					});
					
					maker.select(id.getPacketList());
					maker.getBlockList().addAll(id.getBlockList().stream().map(Location::getBlock).collect(Collectors.toList()));
					id.setSQLAction(SQLAction.REMOVE);
					return;
				}
			}
		}
		return;
	}
	
	private Project isExist(String s){
		for(Project project : FurnitureLib.getInstance().getFurnitureManager().getProjects()){
			if(project.getName().toLowerCase().equalsIgnoreCase(s.toLowerCase())){
				return project;
			}
		}
		return null;
	}
}
