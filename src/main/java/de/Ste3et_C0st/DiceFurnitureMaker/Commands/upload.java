package de.Ste3et_C0st.DiceFurnitureMaker.Commands;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.Ste3et_C0st.DiceFunitureMaker.FurnitureMakerPlugin;
import de.Ste3et_C0st.FurnitureLib.Command.command;
import de.Ste3et_C0st.FurnitureLib.Command.iCommand;
import de.Ste3et_C0st.FurnitureLib.Crafting.CraftingFile;
import de.Ste3et_C0st.FurnitureLib.Crafting.Project;
import de.Ste3et_C0st.FurnitureLib.NBT.CraftItemStack;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTCompressedStreamTools;
import de.Ste3et_C0st.FurnitureLib.NBT.NBTTagCompound;
import de.Ste3et_C0st.FurnitureLib.Utilitis.HiddenStringUtils;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.Type.PlaceableSide;

public class upload extends iCommand {
	
	public upload(String subCommand, String ...args) {
		super(subCommand);
		setTab("installedDModels");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof Player){
			if(args.length!=2){command.sendHelp((Player) sender);return;}
			if(args[0].equalsIgnoreCase("upload")){
				try{
					if(!hasCommandPermission(sender)) return;
					final URL url = new URL("http://api.dicecraft.de/furniture/upload.php");
					String name = args[1];
					Project project = isExist(name);
					sender.sendMessage("§7§m+--------------------§7[§2Upload§7]§m---------------------+");
					sender.sendMessage("§6Upload startet from: " + name);
					uploadData(project, sender, url, (Player) sender);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
	private void uploadData(final Project project, final CommandSender sender, final URL url, final Player p){
		new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					URLConnection connection = (URLConnection) url.openConnection();
					connection.setRequestProperty("User-Agent", "FurnitureMaker/" + FurnitureMakerPlugin.getInstance().getDescription().getVersion());
					connection.setDoOutput(true);
					connection.setDoInput(true);

					PrintStream stream = new PrintStream(connection.getOutputStream());
					String user = sender.getName();
					String config = getMetadata(project);
					if(config==null){
						sender.sendMessage("§cA internal error has been generated");
						sender.sendMessage("§7§m+------------------------------------------------+");
						return;
					}
					String projectString = project.getName();
					stream.println("user=" + user);
					stream.println("&config=" + config);
					stream.println("&projectString=" + projectString);
					stream.println("&uuid=" + p.getUniqueId().toString());
					stream.println("&spigot=" + ("1." + FurnitureLib.getVersionInt()));
					
					BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					
					stream.checkError();
					stream.flush();
					stream.close();
					
					String line = null;
					String password = "";
					while ((line = reader.readLine()) != null) {
						if(line.startsWith("@")){
							String[] split = line.split("#");
							((Player) sender).spigot().sendMessage(
							new ComponentBuilder(split[0].replaceFirst("@", "") + " ").append("§6Click Here")
							.event(new ClickEvent(ClickEvent.Action.OPEN_URL, split[1])).color(net.md_5.bungee.api.ChatColor.DARK_GREEN).create());
						}else{
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', line));
							if(line.contains("Password: ")) {
								int i = line.lastIndexOf("Password: ");
								password = line.substring(i).replace("Password: ", "");
							}
						}
					}
					connection.getInputStream().close();
					
					if(!password.isEmpty()) {
						File folder = new File("plugins/FurnitureLib/plugin/DiceEditor/");
						if(!folder.exists()) folder.mkdirs();
						File file = new File(folder, sender.getName() + ".yml");
						YamlConfiguration passwordConfig = new YamlConfiguration();
						passwordConfig.set("username", sender.getName());
						passwordConfig.set("password", password);
						passwordConfig.save(file);
						sender.sendMessage("§6UserData §7file is saved to:");
						sender.sendMessage("§7/plugins/FurnitureLib/plugin/DiceEditor/" + sender.getName() + ".yml");
					}
					
					sender.sendMessage("§7§m+------------------------------------------------+");
				}catch(Exception e){
					sender.sendMessage("§7§m+----------------------------------------------+");
					e.printStackTrace();
					return;
				}
			}
		}).start();
	}
	
	private Project isExist(String s){
		for(Project project : FurnitureLib.getInstance().getFurnitureManager().getProjects()){
			if(project.getName().equalsIgnoreCase(s)){
				return project;
			}
		}
		return null;
	}
	
	public String getHeader(YamlConfiguration file){
		return (String) file.getConfigurationSection("").getKeys(false).toArray()[0];
	}
	
	public String getMetadata(Project pro){
		try{
			YamlConfiguration config = new YamlConfiguration();
			if(FurnitureLib.isNewVersion()) {
				config.load(new File("plugins/FurnitureLib/models/", pro.getName()+".dModel"));
			}else {
				config.load(new File("plugins/FurnitureLib/Crafting/", pro.getName()+".yml"));
			}
			String header = getHeader(config);
			NBTTagCompound compound = new NBTTagCompound();
			
			compound.setString("system-ID", header);
			
			if(config.contains(header + ".creator")) compound.setString("creator", config.getString(header + ".creator"));
			
			NBTTagCompound lore = new NBTTagCompound();
			int i  = 0;
			for(String str : config.getStringList(header + (FurnitureLib.isNewVersion() ? ".itemLore" : ".lore"))){
				lore.setString(i+"", str);
				i++;
			}
			NBTTagCompound crafting = new NBTTagCompound();
			crafting.setBoolean("disable", config.getBoolean(header+".crafting.disable"));
			crafting.setString("recipe", config.getString(header+".crafting.recipe"));
			
			NBTTagCompound index = new NBTTagCompound();
			config.getConfigurationSection(header+".crafting.index").getKeys(false).forEach(letter -> {
				index.setString(letter, config.getString(header+".crafting.index." + letter));
			});

			crafting.set("index", index);
			compound.set("crafting", crafting);
			
			if(FurnitureLib.isNewVersion()) {
				compound.setString("displayName", config.getString(header + ".displayName"));
				compound.setString("spawnMaterial", config.getString(header + ".spawnMaterial"));
				compound.setBoolean("itemGlowEffect", config.getBoolean(header + ".itemGlowEffect"));
				compound.set("itemLore", lore);
				if(config.contains(header + ".spawnItemStack")) {
					ItemStack stack = config.getItemStack(header + ".spawnItemStack");
					compound.set("spawnItemStack", new CraftItemStack().getNBTTag(stack));
				}
			}else {
				compound.setString("name", config.getString(header + ".name"));
				compound.setBoolean("glow", config.getBoolean(header + ".glow"));
				compound.set("lore", lore);
				if(config.contains(header + ".material")) {
					String material = config.getString(header + ".material", FurnitureLib.getInstance().getDefaultSpawnMaterial().name());
					try {
						compound.setInt("material", Integer.parseInt(material));
					}catch (Exception e) {
						compound.setString("material", material);
					}
				}
			}
			
			if(FurnitureLib.isNewVersion()) {
				if(config.isConfigurationSection(header+".projectData.entitys")){
					if(config.isSet(header+".projectData.entitys")){
						NBTTagCompound armorStands = new NBTTagCompound();
						config.getConfigurationSection(header+".projectData.entitys").getKeys(false).forEach(letter -> {
							armorStands.setString(letter, config.getString(header+".projectData.entitys."+letter));
						});
						compound.set("entitys", armorStands);
					}
				}else if(config.isConfigurationSection(header+".projectData.entities")) {
					if(config.isSet(header+".projectData.entities")){
						NBTTagCompound armorStands = new NBTTagCompound();
						config.getConfigurationSection(header+".projectData.entities").getKeys(false).forEach(letter -> {
							armorStands.setString(letter, config.getString(header+".projectData.entities."+letter));
						});
						compound.set("entities", armorStands);
					}
				}
			}else {
				if(config.isConfigurationSection(header+".ProjectModels.ArmorStands")){
					if(config.isSet(header+".ProjectModels.ArmorStands")){
						NBTTagCompound armorStands = new NBTTagCompound();
						for(String str : config.getConfigurationSection(header+".ProjectModels.ArmorStands").getKeys(false)){
							armorStands.setString(str, config.getString(header+".ProjectModels.ArmorStands."+str));
						}
						compound.set("ArmorStands", armorStands);
					}
				}
			}
			
			
			if(config.isSet("placeAbleSide")){
				compound.setString("placeAbleSide", PlaceableSide.valueOf(config.getString("placeAbleSide")).toString());
			}else{
				compound.setString("placeAbleSide", PlaceableSide.TOP.toString());
			}
			
			if(FurnitureLib.isNewVersion()) {
				if(config.isConfigurationSection(header+".projectData.blockList")){
					if(config.isSet(header+".projectData.blockList")){
						NBTTagCompound blockList = new NBTTagCompound();
						config.getConfigurationSection(header+".projectData.blockList").getKeys(false).stream().forEach(letter -> {
							NBTTagCompound block = new NBTTagCompound();
							block.setDouble("xOffset", config.getDouble(header+".projectData.blockList." + letter + ".xOffset"));
							block.setDouble("yOffset", config.getDouble(header+".projectData.blockList." + letter + ".yOffset"));
							block.setDouble("zOffset", config.getDouble(header+".projectData.blockList." + letter + ".zOffset"));
							
							String blockData = "";
							if(config.contains(header+".projectData.blockList." + letter + ".blockData")) {
								blockData = config.getString(header+".projectData.blockList." + letter + ".blockData").toLowerCase();
							}else {
								blockData += "minecraft:" + config.getString(header+".projectData.blockList." + letter + ".material").toLowerCase();
								if(config.contains(header+".projectData.blockList." + letter + ".Rotation")) {
									blockData += "[facing="+config.getString(header+".projectData.blockList." + letter + ".Rotation");
								}
							}
							block.setString("blockData", blockData.toLowerCase());
							
							blockList.set(letter, block);
						});
						compound.set("blockList", blockList);
					}
				}
			}else {
				if(config.isConfigurationSection(header+".ProjectModels.Block")){
					if(config.isSet(header+".ProjectModels.Block")){
						NBTTagCompound blockList = new NBTTagCompound();
						for(String str : config.getConfigurationSection(header+".ProjectModels.Block").getKeys(false)){
							NBTTagCompound block = new NBTTagCompound();
							block.setDouble("X-Offset", config.getDouble(header+".ProjectModels.Block." + str + ".X-Offset"));
							block.setDouble("Y-Offset", config.getDouble(header+".ProjectModels.Block." + str + ".Y-Offset"));
							block.setDouble("Z-Offset", config.getDouble(header+".ProjectModels.Block." + str + ".Z-Offset"));
							block.setString("Type", config.getString(header+".ProjectModels.Block." + str + ".Type"));
							block.setInt("Data", config.getInt(header+".ProjectModels.Block." + str + ".Data"));
							
							if(config.contains(header+".ProjectModels.Block." + str + ".gameProfile")) {
								NBTTagCompound profile = new NBTTagCompound();
								profile.setString("uuid", config.getString(header+".ProjectModels.Block." + str + ".gameProfile.uuid"));
								profile.setString("texture_value", config.getString(header+".ProjectModels.Block." + str + ".gameProfile.textures.value"));
								profile.setString("texture_signature", config.getString(header+".ProjectModels.Block." + str + ".gameProfile.textures.signature"));
								block.set("gameProfile", profile);
							}
							
							blockList.set(str, block);
						}
						compound.set("Blocks", blockList);
					}
				}
			}
			
			
			if(config.contains(header+".projectData.functions")) {
				List<String> stringList = config.getStringList(header+".projectData.functions");
				NBTTagCompound functions = new NBTTagCompound();
				int j = 0;
				for(String function : stringList) {
					functions.setString(j + "", function);
					j++;
				}
				if(!functions.isEmpty()) compound.set("function", functions);
			}
			
			return Base64.getUrlEncoder().encodeToString(getByte(compound));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	  public byte[] getByte(NBTTagCompound compound)
	  {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    try
	    {
	      NBTCompressedStreamTools.write(compound, out);
	      out.close();
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	      return new byte[0];
	    }
	    return out.toByteArray();
	  }
}
